import { atom, selector } from "recoil";
import { Inventar, QuestListItem, User, UserConfig } from "./gen/api";
import { Ranking } from "./gen/api/models/Ranking";
import { TodoListItem } from "./gen/api/models/TodoListItem";

export interface IUserState {
  authenticated: boolean;
  data?: User;
}

const backgroundImagesAll = ["1"];
const profileImagesAll = ["1", "2", "3", "4", "5"];

// user states
export const userState = atom<IUserState>({
  key: "UserState",
  default: {
    authenticated: false,
    data: undefined,
  },
});

export const rankingState = atom<Ranking>({
  key: "RankingState",
  default: {
    ownRank: "-",
    users: [],
  },
});

export const configState = atom<UserConfig>({
  key: "ConfigState",
  default: {
    backgroundImageId: "0",
    profileImageId: "0",
  },
});

// Popup states
export const popupActiveState = atom<boolean>({
  key: "PopupActiveState",
  default: false,
});

export const popupContentState = atom<{}>({
  key: "PopupContentState",
  default: {},
});

// shop and inventory states
export const inventoryState = atom<Inventar>({
  key: "InventoryState",
  default: {
    backgroundImageInventar: [],
    profileImageInventar: [],
  },
});

export const backgroundImageShopItems = selector({
  key: "BackgroundImageShopItems",
  get: ({ get }) => {
    const items = get(inventoryState).backgroundImageInventar;
    const result = backgroundImagesAll.filter(
      (id1) => !items.some((id2) => id2 === id1)
    );
    return result;
  },
});

export const profileImageShopItems = selector({
  key: "ProfileImageShopItems",
  get: ({ get }) => {
    const items = get(inventoryState).profileImageInventar;
    const result = profileImagesAll.filter(
      (id1) => !items.some((id2) => id2 === id1)
    );
    return result;
  },
});

// quest and todo states
export const questsState = atom<QuestListItem[]>({
  key: "QuestsState",
  default: [],
});

export const todosState = atom<TodoListItem[]>({
  key: "TodoState",
  default: [],
});

export const activeTodosState = selector({
  key: "ActiveTodosState",
  get: ({ get }) => {
    const todos = get(todosState);
    return todos
      .filter((todo) => todo.status === "ACTIVE")
      .sort((a, b) =>
        Date.parse(a.updatedAt) < Date.parse(b.updatedAt) ? -1 : 1
      );
  },
});

export const plannedTodosState = selector({
  key: "PlannedTodosState",
  get: ({ get }) => {
    const todos = get(todosState);
    return todos
      .filter((todo) => todo.status === "PLANNED")
      .sort((a, b) =>
        Date.parse(a.updatedAt) < Date.parse(b.updatedAt) ? -1 : 1
      );
  },
});

export const finishedTodosState = selector({
  key: "FinishedTodosState",
  get: ({ get }) => {
    const todos = get(todosState);
    return todos
      .filter((todo) => todo.status === "FINISHED")
      .sort((a, b) =>
        Date.parse(a.updatedAt) > Date.parse(b.updatedAt) ? -1 : 1
      );
  },
});
