/**
 * Sample API
 * Web application to to work on your habits with frequently selected quests
 *
 * OpenAPI spec version: 0.1
 * Contact: 76628@studmail.htw-aalen.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { HttpFile } from "../http/http";

/**
 * Indicate the frequency of a todo
 */
export type FrequencyEnum =
  | "DAILY"
  | "TWO_DAYS"
  | "THREE_DAYS"
  | "WEEKLY"
  | "TWO_WEEKS";
