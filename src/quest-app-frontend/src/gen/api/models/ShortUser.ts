/**
 * Sample API
 * Web application to to work on your habits with frequently selected quests
 *
 * OpenAPI spec version: 0.1
 * Contact: 76628@studmail.htw-aalen.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { HttpFile } from "../http/http";

export class ShortUser {
  "playerName": string;
  "email": string;
  "level": number;
  "activeQuests": number;
  "finishedQuests": number;
  "archenemy"?: string;

  static readonly discriminator: string | undefined = undefined;

  static readonly attributeTypeMap: Array<{
    name: string;
    baseName: string;
    type: string;
    format: string;
  }> = [
    {
      name: "playerName",
      baseName: "playerName",
      type: "string",
      format: "",
    },
    {
      name: "email",
      baseName: "email",
      type: "string",
      format: "email",
    },
    {
      name: "level",
      baseName: "level",
      type: "number",
      format: "int32",
    },
    {
      name: "activeQuests",
      baseName: "activeQuests",
      type: "number",
      format: "int32",
    },
    {
      name: "finishedQuests",
      baseName: "finishedQuests",
      type: "number",
      format: "int32",
    },
    {
      name: "archenemy",
      baseName: "archenemy",
      type: "string",
      format: "",
    },
  ];

  static getAttributeTypeMap() {
    return ShortUser.attributeTypeMap;
  }

  public constructor() {}
}
