import { ResponseContext, RequestContext, HttpFile } from "../http/http";
import * as models from "../models/all";
import { Configuration } from "../configuration";

import { CreateUser } from "../models/CreateUser";
import { Credentials } from "../models/Credentials";
import { FrequencyEnum } from "../models/FrequencyEnum";
import { Inventar } from "../models/Inventar";
import { ModelError } from "../models/ModelError";
import { Quest } from "../models/Quest";
import { QuestListItem } from "../models/QuestListItem";
import { RankUser } from "../models/RankUser";
import { Ranking } from "../models/Ranking";
import { Reward } from "../models/Reward";
import { ShortUser } from "../models/ShortUser";
import { StatusEnum } from "../models/StatusEnum";
import { Todo } from "../models/Todo";
import { TodoListItem } from "../models/TodoListItem";
import { TokenResponse } from "../models/TokenResponse";
import { UpdateUser } from "../models/UpdateUser";
import { User } from "../models/User";
import { UserConfig } from "../models/UserConfig";
import { ObservableAuthorizationApi } from "./ObservableAPI";

import {
  AuthorizationApiRequestFactory,
  AuthorizationApiResponseProcessor,
} from "../apis/AuthorizationApi";

import { ObservableMarketApi } from "./ObservableAPI";

import {
  MarketApiRequestFactory,
  MarketApiResponseProcessor,
} from "../apis/MarketApi";

import { ObservableQuestApi } from "./ObservableAPI";

import {
  QuestApiRequestFactory,
  QuestApiResponseProcessor,
} from "../apis/QuestApi";

import { ObservableUserApi } from "./ObservableAPI";

import {
  UserApiRequestFactory,
  UserApiResponseProcessor,
} from "../apis/UserApi";
export class PromiseAuthorizationApi {
  private api: ObservableAuthorizationApi;

  public constructor(
    configuration: Configuration,
    requestFactory?: AuthorizationApiRequestFactory,
    responseProcessor?: AuthorizationApiResponseProcessor
  ) {
    this.api = new ObservableAuthorizationApi(
      configuration,
      requestFactory,
      responseProcessor
    );
  }

  /**
   * Uses username and password to log in a user.
   * @param credentials
   */
  public authLoginPost(
    credentials: Credentials,
    options?: Configuration
  ): Promise<TokenResponse> {
    const result = this.api.authLoginPost(credentials, options);
    return result.toPromise();
  }
}
export class PromiseMarketApi {
  private api: ObservableMarketApi;

  public constructor(
    configuration: Configuration,
    requestFactory?: MarketApiRequestFactory,
    responseProcessor?: MarketApiResponseProcessor
  ) {
    this.api = new ObservableMarketApi(
      configuration,
      requestFactory,
      responseProcessor
    );
  }

  /**
   * Get all assets of user.
   */
  public marketGet(options?: Configuration): Promise<Inventar> {
    const result = this.api.marketGet(options);
    return result.toPromise();
  }

  /**
   * Add asset to user inventar.
   * @param price price of the shop item
   * @param backgroundImageId ID of a background image
   * @param profileImageId ID of a profile image
   */
  public marketPut(
    price: string,
    backgroundImageId?: string,
    profileImageId?: string,
    options?: Configuration
  ): Promise<void> {
    const result = this.api.marketPut(
      price,
      backgroundImageId,
      profileImageId,
      options
    );
    return result.toPromise();
  }
}
export class PromiseQuestApi {
  private api: ObservableQuestApi;

  public constructor(
    configuration: Configuration,
    requestFactory?: QuestApiRequestFactory,
    responseProcessor?: QuestApiResponseProcessor
  ) {
    this.api = new ObservableQuestApi(
      configuration,
      requestFactory,
      responseProcessor
    );
  }

  /**
   * Get all available quests.
   * @param nameFilter Name to filter
   */
  public questsGet(
    nameFilter?: string,
    options?: Configuration
  ): Promise<Array<QuestListItem>> {
    const result = this.api.questsGet(nameFilter, options);
    return result.toPromise();
  }

  /**
   * Get specific quest.
   * @param questId ID of a quest.
   */
  public questsQuestIdGet(
    questId: string,
    options?: Configuration
  ): Promise<Quest> {
    const result = this.api.questsQuestIdGet(questId, options);
    return result.toPromise();
  }

  /**
   * Add a quest to your todos.
   * @param questId ID of a quest.
   */
  public questsTodosAddQuestIdPost(
    questId: string,
    options?: Configuration
  ): Promise<void> {
    const result = this.api.questsTodosAddQuestIdPost(questId, options);
    return result.toPromise();
  }

  /**
   * Get all todo´s for user.
   */
  public questsTodosGet(options?: Configuration): Promise<Array<TodoListItem>> {
    const result = this.api.questsTodosGet(options);
    return result.toPromise();
  }

  /**
   * Close todo with id. Status of todo changes from active to completed.
   * @param todoId ID of a todo.
   */
  public questsTodosTodoIdClosePut(
    todoId: string,
    options?: Configuration
  ): Promise<void> {
    const result = this.api.questsTodosTodoIdClosePut(todoId, options);
    return result.toPromise();
  }

  /**
   * Delete a todo. Your current process on this todo will be deleted. Finished todos can not be deleted.
   * @param todoId ID of a todo.
   */
  public questsTodosTodoIdDelete(
    todoId: string,
    options?: Configuration
  ): Promise<void> {
    const result = this.api.questsTodosTodoIdDelete(todoId, options);
    return result.toPromise();
  }

  /**
   * Finish todo with id. You get reward from this action.
   * @param todoId ID of a todo.
   */
  public questsTodosTodoIdFinishPut(
    todoId: string,
    options?: Configuration
  ): Promise<Reward> {
    const result = this.api.questsTodosTodoIdFinishPut(todoId, options);
    return result.toPromise();
  }

  /**
   * Get specific todo.
   * @param todoId ID of a todo.
   */
  public questsTodosTodoIdGet(
    todoId: string,
    options?: Configuration
  ): Promise<Todo> {
    const result = this.api.questsTodosTodoIdGet(todoId, options);
    return result.toPromise();
  }

  /**
   * Start to work on a todo. Status of todo changes from planned to active.
   * @param todoId ID of a todo.
   * @param frequency Frequency for this todo. If this or startDate is not passed, todo will be remain with status &#39;PLANNED&#39;.
   */
  public questsTodosTodoIdStartPut(
    todoId: string,
    frequency?: FrequencyEnum,
    options?: Configuration
  ): Promise<void> {
    const result = this.api.questsTodosTodoIdStartPut(
      todoId,
      frequency,
      options
    );
    return result.toPromise();
  }
}
export class PromiseUserApi {
  private api: ObservableUserApi;

  public constructor(
    configuration: Configuration,
    requestFactory?: UserApiRequestFactory,
    responseProcessor?: UserApiResponseProcessor
  ) {
    this.api = new ObservableUserApi(
      configuration,
      requestFactory,
      responseProcessor
    );
  }

  /**
   * Get user by player name.
   * @param playerName Player name of a specific user.
   */
  public userByNamePlayerNameGet(
    playerName: string,
    options?: Configuration
  ): Promise<ShortUser> {
    const result = this.api.userByNamePlayerNameGet(playerName, options);
    return result.toPromise();
  }

  /**
   * Get user config.
   */
  public userConfigGet(options?: Configuration): Promise<UserConfig> {
    const result = this.api.userConfigGet(options);
    return result.toPromise();
  }

  /**
   * Change user config.
   * @param userConfig
   */
  public userConfigPut(
    userConfig?: UserConfig,
    options?: Configuration
  ): Promise<void> {
    const result = this.api.userConfigPut(userConfig, options);
    return result.toPromise();
  }

  /**
   * Get user information.
   */
  public userGet(options?: Configuration): Promise<User> {
    const result = this.api.userGet(options);
    return result.toPromise();
  }

  /**
   * Create a new user.
   * @param createUser
   */
  public userPost(
    createUser?: CreateUser,
    options?: Configuration
  ): Promise<void> {
    const result = this.api.userPost(createUser, options);
    return result.toPromise();
  }

  /**
   * Change user information. Choose archenemy.
   * @param updateUser
   */
  public userPut(
    updateUser?: UpdateUser,
    options?: Configuration
  ): Promise<User> {
    const result = this.api.userPut(updateUser, options);
    return result.toPromise();
  }

  /**
   * Get rank list of the desired category.
   */
  public userRanksGet(options?: Configuration): Promise<Ranking> {
    const result = this.api.userRanksGet(options);
    return result.toPromise();
  }
}
