import { ResponseContext, RequestContext, HttpFile } from "../http/http";
import * as models from "../models/all";
import { Configuration } from "../configuration";

import { CreateUser } from "../models/CreateUser";
import { Credentials } from "../models/Credentials";
import { FrequencyEnum } from "../models/FrequencyEnum";
import { Inventar } from "../models/Inventar";
import { ModelError } from "../models/ModelError";
import { Quest } from "../models/Quest";
import { QuestListItem } from "../models/QuestListItem";
import { RankUser } from "../models/RankUser";
import { Ranking } from "../models/Ranking";
import { Reward } from "../models/Reward";
import { ShortUser } from "../models/ShortUser";
import { StatusEnum } from "../models/StatusEnum";
import { Todo } from "../models/Todo";
import { TodoListItem } from "../models/TodoListItem";
import { TokenResponse } from "../models/TokenResponse";
import { UpdateUser } from "../models/UpdateUser";
import { User } from "../models/User";
import { UserConfig } from "../models/UserConfig";

import { ObservableAuthorizationApi } from "./ObservableAPI";
import {
  AuthorizationApiRequestFactory,
  AuthorizationApiResponseProcessor,
} from "../apis/AuthorizationApi";

import { ObservableMarketApi } from "./ObservableAPI";
import {
  MarketApiRequestFactory,
  MarketApiResponseProcessor,
} from "../apis/MarketApi";

import { ObservableQuestApi } from "./ObservableAPI";
import {
  QuestApiRequestFactory,
  QuestApiResponseProcessor,
} from "../apis/QuestApi";

import { ObservableUserApi } from "./ObservableAPI";
import {
  UserApiRequestFactory,
  UserApiResponseProcessor,
} from "../apis/UserApi";

export interface AuthorizationApiAuthLoginPostRequest {
  /**
   *
   * @type Credentials
   * @memberof AuthorizationApiauthLoginPost
   */
  credentials: Credentials;
}

export class ObjectAuthorizationApi {
  private api: ObservableAuthorizationApi;

  public constructor(
    configuration: Configuration,
    requestFactory?: AuthorizationApiRequestFactory,
    responseProcessor?: AuthorizationApiResponseProcessor
  ) {
    this.api = new ObservableAuthorizationApi(
      configuration,
      requestFactory,
      responseProcessor
    );
  }

  /**
   * Uses username and password to log in a user.
   * @param param the request object
   */
  public authLoginPost(
    param: AuthorizationApiAuthLoginPostRequest,
    options?: Configuration
  ): Promise<TokenResponse> {
    return this.api.authLoginPost(param.credentials, options).toPromise();
  }
}

export interface MarketApiMarketGetRequest {}

export interface MarketApiMarketPutRequest {
  /**
   * price of the shop item
   * @type string
   * @memberof MarketApimarketPut
   */
  price: string;
  /**
   * ID of a background image
   * @type string
   * @memberof MarketApimarketPut
   */
  backgroundImageId?: string;
  /**
   * ID of a profile image
   * @type string
   * @memberof MarketApimarketPut
   */
  profileImageId?: string;
}

export class ObjectMarketApi {
  private api: ObservableMarketApi;

  public constructor(
    configuration: Configuration,
    requestFactory?: MarketApiRequestFactory,
    responseProcessor?: MarketApiResponseProcessor
  ) {
    this.api = new ObservableMarketApi(
      configuration,
      requestFactory,
      responseProcessor
    );
  }

  /**
   * Get all assets of user.
   * @param param the request object
   */
  public marketGet(
    param: MarketApiMarketGetRequest,
    options?: Configuration
  ): Promise<Inventar> {
    return this.api.marketGet(options).toPromise();
  }

  /**
   * Add asset to user inventar.
   * @param param the request object
   */
  public marketPut(
    param: MarketApiMarketPutRequest,
    options?: Configuration
  ): Promise<void> {
    return this.api
      .marketPut(
        param.price,
        param.backgroundImageId,
        param.profileImageId,
        options
      )
      .toPromise();
  }
}

export interface QuestApiQuestsGetRequest {
  /**
   * Name to filter
   * @type string
   * @memberof QuestApiquestsGet
   */
  nameFilter?: string;
}

export interface QuestApiQuestsQuestIdGetRequest {
  /**
   * ID of a quest.
   * @type string
   * @memberof QuestApiquestsQuestIdGet
   */
  questId: string;
}

export interface QuestApiQuestsTodosAddQuestIdPostRequest {
  /**
   * ID of a quest.
   * @type string
   * @memberof QuestApiquestsTodosAddQuestIdPost
   */
  questId: string;
}

export interface QuestApiQuestsTodosGetRequest {}

export interface QuestApiQuestsTodosTodoIdClosePutRequest {
  /**
   * ID of a todo.
   * @type string
   * @memberof QuestApiquestsTodosTodoIdClosePut
   */
  todoId: string;
}

export interface QuestApiQuestsTodosTodoIdDeleteRequest {
  /**
   * ID of a todo.
   * @type string
   * @memberof QuestApiquestsTodosTodoIdDelete
   */
  todoId: string;
}

export interface QuestApiQuestsTodosTodoIdFinishPutRequest {
  /**
   * ID of a todo.
   * @type string
   * @memberof QuestApiquestsTodosTodoIdFinishPut
   */
  todoId: string;
}

export interface QuestApiQuestsTodosTodoIdGetRequest {
  /**
   * ID of a todo.
   * @type string
   * @memberof QuestApiquestsTodosTodoIdGet
   */
  todoId: string;
}

export interface QuestApiQuestsTodosTodoIdStartPutRequest {
  /**
   * ID of a todo.
   * @type string
   * @memberof QuestApiquestsTodosTodoIdStartPut
   */
  todoId: string;
  /**
   * Frequency for this todo. If this or startDate is not passed, todo will be remain with status &#39;PLANNED&#39;.
   * @type FrequencyEnum
   * @memberof QuestApiquestsTodosTodoIdStartPut
   */
  frequency?: FrequencyEnum;
}

export class ObjectQuestApi {
  private api: ObservableQuestApi;

  public constructor(
    configuration: Configuration,
    requestFactory?: QuestApiRequestFactory,
    responseProcessor?: QuestApiResponseProcessor
  ) {
    this.api = new ObservableQuestApi(
      configuration,
      requestFactory,
      responseProcessor
    );
  }

  /**
   * Get all available quests.
   * @param param the request object
   */
  public questsGet(
    param: QuestApiQuestsGetRequest,
    options?: Configuration
  ): Promise<Array<QuestListItem>> {
    return this.api.questsGet(param.nameFilter, options).toPromise();
  }

  /**
   * Get specific quest.
   * @param param the request object
   */
  public questsQuestIdGet(
    param: QuestApiQuestsQuestIdGetRequest,
    options?: Configuration
  ): Promise<Quest> {
    return this.api.questsQuestIdGet(param.questId, options).toPromise();
  }

  /**
   * Add a quest to your todos.
   * @param param the request object
   */
  public questsTodosAddQuestIdPost(
    param: QuestApiQuestsTodosAddQuestIdPostRequest,
    options?: Configuration
  ): Promise<void> {
    return this.api
      .questsTodosAddQuestIdPost(param.questId, options)
      .toPromise();
  }

  /**
   * Get all todo´s for user.
   * @param param the request object
   */
  public questsTodosGet(
    param: QuestApiQuestsTodosGetRequest,
    options?: Configuration
  ): Promise<Array<TodoListItem>> {
    return this.api.questsTodosGet(options).toPromise();
  }

  /**
   * Close todo with id. Status of todo changes from active to completed.
   * @param param the request object
   */
  public questsTodosTodoIdClosePut(
    param: QuestApiQuestsTodosTodoIdClosePutRequest,
    options?: Configuration
  ): Promise<void> {
    return this.api
      .questsTodosTodoIdClosePut(param.todoId, options)
      .toPromise();
  }

  /**
   * Delete a todo. Your current process on this todo will be deleted. Finished todos can not be deleted.
   * @param param the request object
   */
  public questsTodosTodoIdDelete(
    param: QuestApiQuestsTodosTodoIdDeleteRequest,
    options?: Configuration
  ): Promise<void> {
    return this.api.questsTodosTodoIdDelete(param.todoId, options).toPromise();
  }

  /**
   * Finish todo with id. You get reward from this action.
   * @param param the request object
   */
  public questsTodosTodoIdFinishPut(
    param: QuestApiQuestsTodosTodoIdFinishPutRequest,
    options?: Configuration
  ): Promise<Reward> {
    return this.api
      .questsTodosTodoIdFinishPut(param.todoId, options)
      .toPromise();
  }

  /**
   * Get specific todo.
   * @param param the request object
   */
  public questsTodosTodoIdGet(
    param: QuestApiQuestsTodosTodoIdGetRequest,
    options?: Configuration
  ): Promise<Todo> {
    return this.api.questsTodosTodoIdGet(param.todoId, options).toPromise();
  }

  /**
   * Start to work on a todo. Status of todo changes from planned to active.
   * @param param the request object
   */
  public questsTodosTodoIdStartPut(
    param: QuestApiQuestsTodosTodoIdStartPutRequest,
    options?: Configuration
  ): Promise<void> {
    return this.api
      .questsTodosTodoIdStartPut(param.todoId, param.frequency, options)
      .toPromise();
  }
}

export interface UserApiUserByNamePlayerNameGetRequest {
  /**
   * Player name of a specific user.
   * @type string
   * @memberof UserApiuserByNamePlayerNameGet
   */
  playerName: string;
}

export interface UserApiUserConfigGetRequest {}

export interface UserApiUserConfigPutRequest {
  /**
   *
   * @type UserConfig
   * @memberof UserApiuserConfigPut
   */
  userConfig?: UserConfig;
}

export interface UserApiUserGetRequest {}

export interface UserApiUserPostRequest {
  /**
   *
   * @type CreateUser
   * @memberof UserApiuserPost
   */
  createUser?: CreateUser;
}

export interface UserApiUserPutRequest {
  /**
   *
   * @type UpdateUser
   * @memberof UserApiuserPut
   */
  updateUser?: UpdateUser;
}

export interface UserApiUserRanksGetRequest {}

export class ObjectUserApi {
  private api: ObservableUserApi;

  public constructor(
    configuration: Configuration,
    requestFactory?: UserApiRequestFactory,
    responseProcessor?: UserApiResponseProcessor
  ) {
    this.api = new ObservableUserApi(
      configuration,
      requestFactory,
      responseProcessor
    );
  }

  /**
   * Get user by player name.
   * @param param the request object
   */
  public userByNamePlayerNameGet(
    param: UserApiUserByNamePlayerNameGetRequest,
    options?: Configuration
  ): Promise<ShortUser> {
    return this.api
      .userByNamePlayerNameGet(param.playerName, options)
      .toPromise();
  }

  /**
   * Get user config.
   * @param param the request object
   */
  public userConfigGet(
    param: UserApiUserConfigGetRequest,
    options?: Configuration
  ): Promise<UserConfig> {
    return this.api.userConfigGet(options).toPromise();
  }

  /**
   * Change user config.
   * @param param the request object
   */
  public userConfigPut(
    param: UserApiUserConfigPutRequest,
    options?: Configuration
  ): Promise<void> {
    return this.api.userConfigPut(param.userConfig, options).toPromise();
  }

  /**
   * Get user information.
   * @param param the request object
   */
  public userGet(
    param: UserApiUserGetRequest,
    options?: Configuration
  ): Promise<User> {
    return this.api.userGet(options).toPromise();
  }

  /**
   * Create a new user.
   * @param param the request object
   */
  public userPost(
    param: UserApiUserPostRequest,
    options?: Configuration
  ): Promise<void> {
    return this.api.userPost(param.createUser, options).toPromise();
  }

  /**
   * Change user information. Choose archenemy.
   * @param param the request object
   */
  public userPut(
    param: UserApiUserPutRequest,
    options?: Configuration
  ): Promise<User> {
    return this.api.userPut(param.updateUser, options).toPromise();
  }

  /**
   * Get rank list of the desired category.
   * @param param the request object
   */
  public userRanksGet(
    param: UserApiUserRanksGetRequest,
    options?: Configuration
  ): Promise<Ranking> {
    return this.api.userRanksGet(options).toPromise();
  }
}
