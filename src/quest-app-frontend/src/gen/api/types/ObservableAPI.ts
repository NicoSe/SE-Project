import { ResponseContext, RequestContext, HttpFile } from "../http/http";
import * as models from "../models/all";
import { Configuration } from "../configuration";
import { Observable, of, from } from "../rxjsStub";
import { mergeMap, map } from "../rxjsStub";
import { CreateUser } from "../models/CreateUser";
import { Credentials } from "../models/Credentials";
import { FrequencyEnum } from "../models/FrequencyEnum";
import { Inventar } from "../models/Inventar";
import { ModelError } from "../models/ModelError";
import { Quest } from "../models/Quest";
import { QuestListItem } from "../models/QuestListItem";
import { RankUser } from "../models/RankUser";
import { Ranking } from "../models/Ranking";
import { Reward } from "../models/Reward";
import { ShortUser } from "../models/ShortUser";
import { StatusEnum } from "../models/StatusEnum";
import { Todo } from "../models/Todo";
import { TodoListItem } from "../models/TodoListItem";
import { TokenResponse } from "../models/TokenResponse";
import { UpdateUser } from "../models/UpdateUser";
import { User } from "../models/User";
import { UserConfig } from "../models/UserConfig";

import {
  AuthorizationApiRequestFactory,
  AuthorizationApiResponseProcessor,
} from "../apis/AuthorizationApi";

import {
  MarketApiRequestFactory,
  MarketApiResponseProcessor,
} from "../apis/MarketApi";

import {
  QuestApiRequestFactory,
  QuestApiResponseProcessor,
} from "../apis/QuestApi";

import {
  UserApiRequestFactory,
  UserApiResponseProcessor,
} from "../apis/UserApi";
export class ObservableAuthorizationApi {
  private requestFactory: AuthorizationApiRequestFactory;
  private responseProcessor: AuthorizationApiResponseProcessor;
  private configuration: Configuration;

  public constructor(
    configuration: Configuration,
    requestFactory?: AuthorizationApiRequestFactory,
    responseProcessor?: AuthorizationApiResponseProcessor
  ) {
    this.configuration = configuration;
    this.requestFactory =
      requestFactory || new AuthorizationApiRequestFactory(configuration);
    this.responseProcessor =
      responseProcessor || new AuthorizationApiResponseProcessor();
  }

  /**
   * Uses username and password to log in a user.
   * @param credentials
   */
  public authLoginPost(
    credentials: Credentials,
    options?: Configuration
  ): Observable<TokenResponse> {
    const requestContextPromise = this.requestFactory.authLoginPost(
      credentials,
      options
    );

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) =>
              this.responseProcessor.authLoginPost(rsp)
            )
          );
        })
      );
  }
}
export class ObservableMarketApi {
  private requestFactory: MarketApiRequestFactory;
  private responseProcessor: MarketApiResponseProcessor;
  private configuration: Configuration;

  public constructor(
    configuration: Configuration,
    requestFactory?: MarketApiRequestFactory,
    responseProcessor?: MarketApiResponseProcessor
  ) {
    this.configuration = configuration;
    this.requestFactory =
      requestFactory || new MarketApiRequestFactory(configuration);
    this.responseProcessor =
      responseProcessor || new MarketApiResponseProcessor();
  }

  /**
   * Get all assets of user.
   */
  public marketGet(options?: Configuration): Observable<Inventar> {
    const requestContextPromise = this.requestFactory.marketGet(options);

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) => this.responseProcessor.marketGet(rsp))
          );
        })
      );
  }

  /**
   * Add asset to user inventar.
   * @param price price of the shop item
   * @param backgroundImageId ID of a background image
   * @param profileImageId ID of a profile image
   */
  public marketPut(
    price: string,
    backgroundImageId?: string,
    profileImageId?: string,
    options?: Configuration
  ): Observable<void> {
    const requestContextPromise = this.requestFactory.marketPut(
      price,
      backgroundImageId,
      profileImageId,
      options
    );

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) => this.responseProcessor.marketPut(rsp))
          );
        })
      );
  }
}
export class ObservableQuestApi {
  private requestFactory: QuestApiRequestFactory;
  private responseProcessor: QuestApiResponseProcessor;
  private configuration: Configuration;

  public constructor(
    configuration: Configuration,
    requestFactory?: QuestApiRequestFactory,
    responseProcessor?: QuestApiResponseProcessor
  ) {
    this.configuration = configuration;
    this.requestFactory =
      requestFactory || new QuestApiRequestFactory(configuration);
    this.responseProcessor =
      responseProcessor || new QuestApiResponseProcessor();
  }

  /**
   * Get all available quests.
   * @param nameFilter Name to filter
   */
  public questsGet(
    nameFilter?: string,
    options?: Configuration
  ): Observable<Array<QuestListItem>> {
    const requestContextPromise = this.requestFactory.questsGet(
      nameFilter,
      options
    );

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) => this.responseProcessor.questsGet(rsp))
          );
        })
      );
  }

  /**
   * Get specific quest.
   * @param questId ID of a quest.
   */
  public questsQuestIdGet(
    questId: string,
    options?: Configuration
  ): Observable<Quest> {
    const requestContextPromise = this.requestFactory.questsQuestIdGet(
      questId,
      options
    );

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) =>
              this.responseProcessor.questsQuestIdGet(rsp)
            )
          );
        })
      );
  }

  /**
   * Add a quest to your todos.
   * @param questId ID of a quest.
   */
  public questsTodosAddQuestIdPost(
    questId: string,
    options?: Configuration
  ): Observable<void> {
    const requestContextPromise = this.requestFactory.questsTodosAddQuestIdPost(
      questId,
      options
    );

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) =>
              this.responseProcessor.questsTodosAddQuestIdPost(rsp)
            )
          );
        })
      );
  }

  /**
   * Get all todo´s for user.
   */
  public questsTodosGet(
    options?: Configuration
  ): Observable<Array<TodoListItem>> {
    const requestContextPromise = this.requestFactory.questsTodosGet(options);

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) =>
              this.responseProcessor.questsTodosGet(rsp)
            )
          );
        })
      );
  }

  /**
   * Close todo with id. Status of todo changes from active to completed.
   * @param todoId ID of a todo.
   */
  public questsTodosTodoIdClosePut(
    todoId: string,
    options?: Configuration
  ): Observable<void> {
    const requestContextPromise = this.requestFactory.questsTodosTodoIdClosePut(
      todoId,
      options
    );

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) =>
              this.responseProcessor.questsTodosTodoIdClosePut(rsp)
            )
          );
        })
      );
  }

  /**
   * Delete a todo. Your current process on this todo will be deleted. Finished todos can not be deleted.
   * @param todoId ID of a todo.
   */
  public questsTodosTodoIdDelete(
    todoId: string,
    options?: Configuration
  ): Observable<void> {
    const requestContextPromise = this.requestFactory.questsTodosTodoIdDelete(
      todoId,
      options
    );

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) =>
              this.responseProcessor.questsTodosTodoIdDelete(rsp)
            )
          );
        })
      );
  }

  /**
   * Finish todo with id. You get reward from this action.
   * @param todoId ID of a todo.
   */
  public questsTodosTodoIdFinishPut(
    todoId: string,
    options?: Configuration
  ): Observable<Reward> {
    const requestContextPromise =
      this.requestFactory.questsTodosTodoIdFinishPut(todoId, options);

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) =>
              this.responseProcessor.questsTodosTodoIdFinishPut(rsp)
            )
          );
        })
      );
  }

  /**
   * Get specific todo.
   * @param todoId ID of a todo.
   */
  public questsTodosTodoIdGet(
    todoId: string,
    options?: Configuration
  ): Observable<Todo> {
    const requestContextPromise = this.requestFactory.questsTodosTodoIdGet(
      todoId,
      options
    );

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) =>
              this.responseProcessor.questsTodosTodoIdGet(rsp)
            )
          );
        })
      );
  }

  /**
   * Start to work on a todo. Status of todo changes from planned to active.
   * @param todoId ID of a todo.
   * @param frequency Frequency for this todo. If this or startDate is not passed, todo will be remain with status &#39;PLANNED&#39;.
   */
  public questsTodosTodoIdStartPut(
    todoId: string,
    frequency?: FrequencyEnum,
    options?: Configuration
  ): Observable<void> {
    const requestContextPromise = this.requestFactory.questsTodosTodoIdStartPut(
      todoId,
      frequency,
      options
    );

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) =>
              this.responseProcessor.questsTodosTodoIdStartPut(rsp)
            )
          );
        })
      );
  }
}
export class ObservableUserApi {
  private requestFactory: UserApiRequestFactory;
  private responseProcessor: UserApiResponseProcessor;
  private configuration: Configuration;

  public constructor(
    configuration: Configuration,
    requestFactory?: UserApiRequestFactory,
    responseProcessor?: UserApiResponseProcessor
  ) {
    this.configuration = configuration;
    this.requestFactory =
      requestFactory || new UserApiRequestFactory(configuration);
    this.responseProcessor =
      responseProcessor || new UserApiResponseProcessor();
  }

  /**
   * Get user by player name.
   * @param playerName Player name of a specific user.
   */
  public userByNamePlayerNameGet(
    playerName: string,
    options?: Configuration
  ): Observable<ShortUser> {
    const requestContextPromise = this.requestFactory.userByNamePlayerNameGet(
      playerName,
      options
    );

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) =>
              this.responseProcessor.userByNamePlayerNameGet(rsp)
            )
          );
        })
      );
  }

  /**
   * Get user config.
   */
  public userConfigGet(options?: Configuration): Observable<UserConfig> {
    const requestContextPromise = this.requestFactory.userConfigGet(options);

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) =>
              this.responseProcessor.userConfigGet(rsp)
            )
          );
        })
      );
  }

  /**
   * Change user config.
   * @param userConfig
   */
  public userConfigPut(
    userConfig?: UserConfig,
    options?: Configuration
  ): Observable<void> {
    const requestContextPromise = this.requestFactory.userConfigPut(
      userConfig,
      options
    );

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) =>
              this.responseProcessor.userConfigPut(rsp)
            )
          );
        })
      );
  }

  /**
   * Get user information.
   */
  public userGet(options?: Configuration): Observable<User> {
    const requestContextPromise = this.requestFactory.userGet(options);

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) => this.responseProcessor.userGet(rsp))
          );
        })
      );
  }

  /**
   * Create a new user.
   * @param createUser
   */
  public userPost(
    createUser?: CreateUser,
    options?: Configuration
  ): Observable<void> {
    const requestContextPromise = this.requestFactory.userPost(
      createUser,
      options
    );

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) => this.responseProcessor.userPost(rsp))
          );
        })
      );
  }

  /**
   * Change user information. Choose archenemy.
   * @param updateUser
   */
  public userPut(
    updateUser?: UpdateUser,
    options?: Configuration
  ): Observable<User> {
    const requestContextPromise = this.requestFactory.userPut(
      updateUser,
      options
    );

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) => this.responseProcessor.userPut(rsp))
          );
        })
      );
  }

  /**
   * Get rank list of the desired category.
   */
  public userRanksGet(options?: Configuration): Observable<Ranking> {
    const requestContextPromise = this.requestFactory.userRanksGet(options);

    // build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    for (let middleware of this.configuration.middleware) {
      middlewarePreObservable = middlewarePreObservable.pipe(
        mergeMap((ctx: RequestContext) => middleware.pre(ctx))
      );
    }

    return middlewarePreObservable
      .pipe(
        mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))
      )
      .pipe(
        mergeMap((response: ResponseContext) => {
          let middlewarePostObservable = of(response);
          for (let middleware of this.configuration.middleware) {
            middlewarePostObservable = middlewarePostObservable.pipe(
              mergeMap((rsp: ResponseContext) => middleware.post(rsp))
            );
          }
          return middlewarePostObservable.pipe(
            map((rsp: ResponseContext) =>
              this.responseProcessor.userRanksGet(rsp)
            )
          );
        })
      );
  }
}
