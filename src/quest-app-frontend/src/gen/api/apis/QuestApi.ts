// TODO: better import syntax?
import { BaseAPIRequestFactory, RequiredError } from "./baseapi";
import { Configuration } from "../configuration";
import {
  RequestContext,
  HttpMethod,
  ResponseContext,
  HttpFile,
} from "../http/http";
import { ObjectSerializer } from "../models/ObjectSerializer";
import { ApiException } from "./exception";
import { isCodeInRange } from "../util";

import { FrequencyEnum } from "../models/FrequencyEnum";
import { Quest } from "../models/Quest";
import { QuestListItem } from "../models/QuestListItem";
import { Reward } from "../models/Reward";
import { Todo } from "../models/Todo";
import { TodoListItem } from "../models/TodoListItem";

/**
 * no description
 */
export class QuestApiRequestFactory extends BaseAPIRequestFactory {
  /**
   * Get all available quests.
   * @param nameFilter Name to filter
   */
  public async questsGet(
    nameFilter?: string,
    options?: Configuration
  ): Promise<RequestContext> {
    let config = options || this.configuration;

    // Path Params
    const localVarPath = "/quests";

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.GET
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params
    if (nameFilter !== undefined) {
      requestContext.setQueryParam(
        "nameFilter",
        ObjectSerializer.serialize(nameFilter, "string", "")
      );
    }

    // Header Params

    // Form Params

    // Body Params

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }

  /**
   * Get specific quest.
   * @param questId ID of a quest.
   */
  public async questsQuestIdGet(
    questId: string,
    options?: Configuration
  ): Promise<RequestContext> {
    let config = options || this.configuration;

    // verify required parameter 'questId' is not null or undefined
    if (questId === null || questId === undefined) {
      throw new RequiredError(
        "Required parameter questId was null or undefined when calling questsQuestIdGet."
      );
    }

    // Path Params
    const localVarPath = "/quests/{quest-id}".replace(
      "{" + "quest-id" + "}",
      encodeURIComponent(String(questId))
    );

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.GET
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params

    // Header Params

    // Form Params

    // Body Params

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }

  /**
   * Add a quest to your todos.
   * @param questId ID of a quest.
   */
  public async questsTodosAddQuestIdPost(
    questId: string,
    options?: Configuration
  ): Promise<RequestContext> {
    let config = options || this.configuration;

    // verify required parameter 'questId' is not null or undefined
    if (questId === null || questId === undefined) {
      throw new RequiredError(
        "Required parameter questId was null or undefined when calling questsTodosAddQuestIdPost."
      );
    }

    // Path Params
    const localVarPath = "/quests/todos/add/{quest-id}".replace(
      "{" + "quest-id" + "}",
      encodeURIComponent(String(questId))
    );

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.POST
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params

    // Header Params

    // Form Params

    // Body Params

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }

  /**
   * Get all todo´s for user.
   */
  public async questsTodosGet(
    options?: Configuration
  ): Promise<RequestContext> {
    let config = options || this.configuration;

    // Path Params
    const localVarPath = "/quests/todos";

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.GET
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params

    // Header Params

    // Form Params

    // Body Params

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }

  /**
   * Close todo with id. Status of todo changes from active to completed.
   * @param todoId ID of a todo.
   */
  public async questsTodosTodoIdClosePut(
    todoId: string,
    options?: Configuration
  ): Promise<RequestContext> {
    let config = options || this.configuration;

    // verify required parameter 'todoId' is not null or undefined
    if (todoId === null || todoId === undefined) {
      throw new RequiredError(
        "Required parameter todoId was null or undefined when calling questsTodosTodoIdClosePut."
      );
    }

    // Path Params
    const localVarPath = "/quests/todos/{todo-id}/close".replace(
      "{" + "todo-id" + "}",
      encodeURIComponent(String(todoId))
    );

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.PUT
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params

    // Header Params

    // Form Params

    // Body Params

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }

  /**
   * Delete a todo. Your current process on this todo will be deleted. Finished todos can not be deleted.
   * @param todoId ID of a todo.
   */
  public async questsTodosTodoIdDelete(
    todoId: string,
    options?: Configuration
  ): Promise<RequestContext> {
    let config = options || this.configuration;

    // verify required parameter 'todoId' is not null or undefined
    if (todoId === null || todoId === undefined) {
      throw new RequiredError(
        "Required parameter todoId was null or undefined when calling questsTodosTodoIdDelete."
      );
    }

    // Path Params
    const localVarPath = "/quests/todos/{todo-id}".replace(
      "{" + "todo-id" + "}",
      encodeURIComponent(String(todoId))
    );

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.DELETE
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params

    // Header Params

    // Form Params

    // Body Params

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }

  /**
   * Finish todo with id. You get reward from this action.
   * @param todoId ID of a todo.
   */
  public async questsTodosTodoIdFinishPut(
    todoId: string,
    options?: Configuration
  ): Promise<RequestContext> {
    let config = options || this.configuration;

    // verify required parameter 'todoId' is not null or undefined
    if (todoId === null || todoId === undefined) {
      throw new RequiredError(
        "Required parameter todoId was null or undefined when calling questsTodosTodoIdFinishPut."
      );
    }

    // Path Params
    const localVarPath = "/quests/todos/{todo-id}/finish".replace(
      "{" + "todo-id" + "}",
      encodeURIComponent(String(todoId))
    );

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.PUT
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params

    // Header Params

    // Form Params

    // Body Params

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }

  /**
   * Get specific todo.
   * @param todoId ID of a todo.
   */
  public async questsTodosTodoIdGet(
    todoId: string,
    options?: Configuration
  ): Promise<RequestContext> {
    let config = options || this.configuration;

    // verify required parameter 'todoId' is not null or undefined
    if (todoId === null || todoId === undefined) {
      throw new RequiredError(
        "Required parameter todoId was null or undefined when calling questsTodosTodoIdGet."
      );
    }

    // Path Params
    const localVarPath = "/quests/todos/{todo-id}".replace(
      "{" + "todo-id" + "}",
      encodeURIComponent(String(todoId))
    );

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.GET
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params

    // Header Params

    // Form Params

    // Body Params

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }

  /**
   * Start to work on a todo. Status of todo changes from planned to active.
   * @param todoId ID of a todo.
   * @param frequency Frequency for this todo. If this or startDate is not passed, todo will be remain with status &#39;PLANNED&#39;.
   */
  public async questsTodosTodoIdStartPut(
    todoId: string,
    frequency?: FrequencyEnum,
    options?: Configuration
  ): Promise<RequestContext> {
    let config = options || this.configuration;

    // verify required parameter 'todoId' is not null or undefined
    if (todoId === null || todoId === undefined) {
      throw new RequiredError(
        "Required parameter todoId was null or undefined when calling questsTodosTodoIdStartPut."
      );
    }

    // Path Params
    const localVarPath = "/quests/todos/{todo-id}/start".replace(
      "{" + "todo-id" + "}",
      encodeURIComponent(String(todoId))
    );

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.PUT
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params
    if (frequency !== undefined) {
      requestContext.setQueryParam(
        "frequency",
        ObjectSerializer.serialize(frequency, "FrequencyEnum", "")
      );
    }

    // Header Params

    // Form Params

    // Body Params

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }
}

export class QuestApiResponseProcessor {
  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to questsGet
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async questsGet(
    response: ResponseContext
  ): Promise<Array<QuestListItem>> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("200", response.httpStatusCode)) {
      const body: Array<QuestListItem> = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Array<QuestListItem>",
        ""
      ) as Array<QuestListItem>;
      return body;
    }
    if (isCodeInRange("400", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(400, body);
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: Array<QuestListItem> = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Array<QuestListItem>",
        ""
      ) as Array<QuestListItem>;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }

  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to questsQuestIdGet
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async questsQuestIdGet(response: ResponseContext): Promise<Quest> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("200", response.httpStatusCode)) {
      const body: Quest = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Quest",
        ""
      ) as Quest;
      return body;
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("404", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "Entity not found."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: Quest = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Quest",
        ""
      ) as Quest;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }

  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to questsTodosAddQuestIdPost
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async questsTodosAddQuestIdPost(
    response: ResponseContext
  ): Promise<void> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("201", response.httpStatusCode)) {
      return;
    }
    if (isCodeInRange("400", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(400, body);
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("404", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "Entity not found."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: void = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "void",
        ""
      ) as void;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }

  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to questsTodosGet
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async questsTodosGet(
    response: ResponseContext
  ): Promise<Array<TodoListItem>> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("200", response.httpStatusCode)) {
      const body: Array<TodoListItem> = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Array<TodoListItem>",
        ""
      ) as Array<TodoListItem>;
      return body;
    }
    if (isCodeInRange("400", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(400, body);
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: Array<TodoListItem> = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Array<TodoListItem>",
        ""
      ) as Array<TodoListItem>;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }

  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to questsTodosTodoIdClosePut
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async questsTodosTodoIdClosePut(
    response: ResponseContext
  ): Promise<void> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("204", response.httpStatusCode)) {
      return;
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("404", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "Entity not found."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: void = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "void",
        ""
      ) as void;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }

  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to questsTodosTodoIdDelete
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async questsTodosTodoIdDelete(
    response: ResponseContext
  ): Promise<void> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("204", response.httpStatusCode)) {
      return;
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("404", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "Entity not found."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: void = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "void",
        ""
      ) as void;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }

  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to questsTodosTodoIdFinishPut
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async questsTodosTodoIdFinishPut(
    response: ResponseContext
  ): Promise<Reward> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("200", response.httpStatusCode)) {
      const body: Reward = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Reward",
        ""
      ) as Reward;
      return body;
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("404", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "Entity not found."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: Reward = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Reward",
        ""
      ) as Reward;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }

  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to questsTodosTodoIdGet
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async questsTodosTodoIdGet(response: ResponseContext): Promise<Todo> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("200", response.httpStatusCode)) {
      const body: Todo = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Todo",
        ""
      ) as Todo;
      return body;
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("404", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "Entity not found."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: Todo = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Todo",
        ""
      ) as Todo;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }

  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to questsTodosTodoIdStartPut
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async questsTodosTodoIdStartPut(
    response: ResponseContext
  ): Promise<void> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("204", response.httpStatusCode)) {
      return;
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("404", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "Entity not found."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: void = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "void",
        ""
      ) as void;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }
}
