// TODO: better import syntax?
import { BaseAPIRequestFactory, RequiredError } from "./baseapi";
import { Configuration } from "../configuration";
import {
  RequestContext,
  HttpMethod,
  ResponseContext,
  HttpFile,
} from "../http/http";
import { ObjectSerializer } from "../models/ObjectSerializer";
import { ApiException } from "./exception";
import { isCodeInRange } from "../util";

import { Inventar } from "../models/Inventar";

/**
 * no description
 */
export class MarketApiRequestFactory extends BaseAPIRequestFactory {
  /**
   * Get all assets of user.
   */
  public async marketGet(options?: Configuration): Promise<RequestContext> {
    let config = options || this.configuration;

    // Path Params
    const localVarPath = "/market";

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.GET
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params

    // Header Params

    // Form Params

    // Body Params

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }

  /**
   * Add asset to user inventar.
   * @param price price of the shop item
   * @param backgroundImageId ID of a background image
   * @param profileImageId ID of a profile image
   */
  public async marketPut(
    price: string,
    backgroundImageId?: string,
    profileImageId?: string,
    options?: Configuration
  ): Promise<RequestContext> {
    let config = options || this.configuration;

    // verify required parameter 'price' is not null or undefined
    if (price === null || price === undefined) {
      throw new RequiredError(
        "Required parameter price was null or undefined when calling marketPut."
      );
    }

    // Path Params
    const localVarPath = "/market";

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.PUT
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params
    if (price !== undefined) {
      requestContext.setQueryParam(
        "price",
        ObjectSerializer.serialize(price, "string", "")
      );
    }
    if (backgroundImageId !== undefined) {
      requestContext.setQueryParam(
        "backgroundImageId",
        ObjectSerializer.serialize(backgroundImageId, "string", "")
      );
    }
    if (profileImageId !== undefined) {
      requestContext.setQueryParam(
        "profileImageId",
        ObjectSerializer.serialize(profileImageId, "string", "")
      );
    }

    // Header Params

    // Form Params

    // Body Params

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }
}

export class MarketApiResponseProcessor {
  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to marketGet
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async marketGet(response: ResponseContext): Promise<Inventar> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("200", response.httpStatusCode)) {
      const body: Inventar = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Inventar",
        ""
      ) as Inventar;
      return body;
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: Inventar = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Inventar",
        ""
      ) as Inventar;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }

  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to marketPut
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async marketPut(response: ResponseContext): Promise<void> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("204", response.httpStatusCode)) {
      return;
    }
    if (isCodeInRange("400", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(400, body);
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: void = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "void",
        ""
      ) as void;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }
}
