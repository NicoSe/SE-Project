// TODO: better import syntax?
import { BaseAPIRequestFactory, RequiredError } from "./baseapi";
import { Configuration } from "../configuration";
import {
  RequestContext,
  HttpMethod,
  ResponseContext,
  HttpFile,
} from "../http/http";
import { ObjectSerializer } from "../models/ObjectSerializer";
import { ApiException } from "./exception";
import { isCodeInRange } from "../util";

import { CreateUser } from "../models/CreateUser";
import { Ranking } from "../models/Ranking";
import { ShortUser } from "../models/ShortUser";
import { UpdateUser } from "../models/UpdateUser";
import { User } from "../models/User";
import { UserConfig } from "../models/UserConfig";

/**
 * no description
 */
export class UserApiRequestFactory extends BaseAPIRequestFactory {
  /**
   * Get user by player name.
   * @param playerName Player name of a specific user.
   */
  public async userByNamePlayerNameGet(
    playerName: string,
    options?: Configuration
  ): Promise<RequestContext> {
    let config = options || this.configuration;

    // verify required parameter 'playerName' is not null or undefined
    if (playerName === null || playerName === undefined) {
      throw new RequiredError(
        "Required parameter playerName was null or undefined when calling userByNamePlayerNameGet."
      );
    }

    // Path Params
    const localVarPath = "/user/byName/{playerName}".replace(
      "{" + "playerName" + "}",
      encodeURIComponent(String(playerName))
    );

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.GET
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params

    // Header Params

    // Form Params

    // Body Params

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }

  /**
   * Get user config.
   */
  public async userConfigGet(options?: Configuration): Promise<RequestContext> {
    let config = options || this.configuration;

    // Path Params
    const localVarPath = "/user/config";

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.GET
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params

    // Header Params

    // Form Params

    // Body Params

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }

  /**
   * Change user config.
   * @param userConfig
   */
  public async userConfigPut(
    userConfig?: UserConfig,
    options?: Configuration
  ): Promise<RequestContext> {
    let config = options || this.configuration;

    // Path Params
    const localVarPath = "/user/config";

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.PUT
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params

    // Header Params

    // Form Params

    // Body Params
    const contentType = ObjectSerializer.getPreferredMediaType([
      "application/json",
    ]);
    requestContext.setHeaderParam("Content-Type", contentType);
    const serializedBody = ObjectSerializer.stringify(
      ObjectSerializer.serialize(userConfig, "UserConfig", ""),
      contentType
    );
    requestContext.setBody(serializedBody);

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }

  /**
   * Get user information.
   */
  public async userGet(options?: Configuration): Promise<RequestContext> {
    let config = options || this.configuration;

    // Path Params
    const localVarPath = "/user";

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.GET
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params

    // Header Params

    // Form Params

    // Body Params

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }

  /**
   * Create a new user.
   * @param createUser
   */
  public async userPost(
    createUser?: CreateUser,
    options?: Configuration
  ): Promise<RequestContext> {
    let config = options || this.configuration;

    // Path Params
    const localVarPath = "/user";

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.POST
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params

    // Header Params

    // Form Params

    // Body Params
    const contentType = ObjectSerializer.getPreferredMediaType([
      "application/json",
    ]);
    requestContext.setHeaderParam("Content-Type", contentType);
    const serializedBody = ObjectSerializer.stringify(
      ObjectSerializer.serialize(createUser, "CreateUser", ""),
      contentType
    );
    requestContext.setBody(serializedBody);

    // Apply auth methods

    return requestContext;
  }

  /**
   * Change user information. Choose archenemy.
   * @param updateUser
   */
  public async userPut(
    updateUser?: UpdateUser,
    options?: Configuration
  ): Promise<RequestContext> {
    let config = options || this.configuration;

    // Path Params
    const localVarPath = "/user";

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.PUT
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params

    // Header Params

    // Form Params

    // Body Params
    const contentType = ObjectSerializer.getPreferredMediaType([
      "application/json",
    ]);
    requestContext.setHeaderParam("Content-Type", contentType);
    const serializedBody = ObjectSerializer.stringify(
      ObjectSerializer.serialize(updateUser, "UpdateUser", ""),
      contentType
    );
    requestContext.setBody(serializedBody);

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }

  /**
   * Get rank list of the desired category.
   */
  public async userRanksGet(options?: Configuration): Promise<RequestContext> {
    let config = options || this.configuration;

    // Path Params
    const localVarPath = "/user/ranks";

    // Make Request Context
    const requestContext = config.baseServer.makeRequestContext(
      localVarPath,
      HttpMethod.GET
    );
    requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8");

    // Query Params

    // Header Params

    // Form Params

    // Body Params

    let authMethod = null;
    // Apply auth methods
    authMethod = config.authMethods["bearerAuth"];
    if (authMethod) {
      await authMethod.applySecurityAuthentication(requestContext);
    }

    return requestContext;
  }
}

export class UserApiResponseProcessor {
  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to userByNamePlayerNameGet
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async userByNamePlayerNameGet(
    response: ResponseContext
  ): Promise<ShortUser> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("200", response.httpStatusCode)) {
      const body: ShortUser = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "ShortUser",
        ""
      ) as ShortUser;
      return body;
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("404", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "Entity not found."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: ShortUser = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "ShortUser",
        ""
      ) as ShortUser;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }

  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to userConfigGet
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async userConfigGet(response: ResponseContext): Promise<UserConfig> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("200", response.httpStatusCode)) {
      const body: UserConfig = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "UserConfig",
        ""
      ) as UserConfig;
      return body;
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: UserConfig = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "UserConfig",
        ""
      ) as UserConfig;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }

  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to userConfigPut
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async userConfigPut(response: ResponseContext): Promise<void> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("204", response.httpStatusCode)) {
      return;
    }
    if (isCodeInRange("400", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(400, body);
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: void = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "void",
        ""
      ) as void;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }

  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to userGet
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async userGet(response: ResponseContext): Promise<User> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("200", response.httpStatusCode)) {
      const body: User = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "User",
        ""
      ) as User;
      return body;
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: User = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "User",
        ""
      ) as User;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }

  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to userPost
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async userPost(response: ResponseContext): Promise<void> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("201", response.httpStatusCode)) {
      return;
    }
    if (isCodeInRange("400", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(400, body);
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: void = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "void",
        ""
      ) as void;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }

  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to userPut
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async userPut(response: ResponseContext): Promise<User> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("200", response.httpStatusCode)) {
      const body: User = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "User",
        ""
      ) as User;
      return body;
    }
    if (isCodeInRange("400", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(400, body);
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("404", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "Entity not found."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: User = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "User",
        ""
      ) as User;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }

  /**
   * Unwraps the actual response sent by the server from the response context and deserializes the response content
   * to the expected objects
   *
   * @params response Response returned by the server for a request to userRanksGet
   * @throws ApiException if the response code was not in [200, 299]
   */
  public async userRanksGet(response: ResponseContext): Promise<Ranking> {
    const contentType = ObjectSerializer.normalizeMediaType(
      response.headers["content-type"]
    );
    if (isCodeInRange("200", response.httpStatusCode)) {
      const body: Ranking = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Ranking",
        ""
      ) as Ranking;
      return body;
    }
    if (isCodeInRange("400", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(400, body);
    }
    if (isCodeInRange("401", response.httpStatusCode)) {
      throw new ApiException<string>(
        response.httpStatusCode,
        "User is not authorized for this request."
      );
    }
    if (isCodeInRange("500", response.httpStatusCode)) {
      const body: Error = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Error",
        ""
      ) as Error;
      throw new ApiException<Error>(500, body);
    }

    // Work around for missing responses in specification, e.g. for petstore.yaml
    if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
      const body: Ranking = ObjectSerializer.deserialize(
        ObjectSerializer.parse(await response.body.text(), contentType),
        "Ranking",
        ""
      ) as Ranking;
      return body;
    }

    let body = response.body || "";
    throw new ApiException<string>(
      response.httpStatusCode,
      'Unknown API Status Code!\nBody: "' + body + '"'
    );
  }
}
