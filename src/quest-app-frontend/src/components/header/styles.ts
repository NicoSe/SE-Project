//===============================================\\
//                Header.tsx                     \\
//===============================================\\

export const headerContainer = {
  marginBottom: "10vh",
};
export const headerMenu = {
  backgroundColor: "rgba(210, 210, 210, 0.7)",
  minHeight: "5vh",
  maxHeight: "10vh",
};
export const headerHeadline = {
  marginLeft: "5vh",
};
export const headerLastMenu = {
  marginRight: "5vw",
};
