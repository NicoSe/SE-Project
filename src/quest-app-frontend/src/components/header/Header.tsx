import { navigate, useLocation } from "@reach/router";
import { useEffect } from "react";
import { useRecoilValue } from "recoil";
import { Container, Menu, Image, Popup, Button } from "semantic-ui-react";
import { configState, userState } from "../../states";
import { translateProfileImageId } from "../helpers";
import {
  headerContainer,
  headerHeadline,
  headerLastMenu,
  headerMenu,
} from "./styles";

export const MainHeader = () => {
  const location = useLocation();
  const user = useRecoilValue(userState);
  const config = useRecoilValue(configState);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location.pathname]);

  const headerClick = () => {
    navigate("/todo");
  };

  return (
    <Container fluid style={headerContainer}>
      <Menu borderless style={headerMenu}>
        <Menu.Item>
          <Popup
            trigger={
              <Button
                size="small"
                style={{ marginLeft: "10px" }}
                color="green"
                icon="question"
              />
            }
            position="bottom left"
            size="large"
            pinned
            on="click"
          >
            Diese Anwednung soll Ihnen dabei helfen, neue Gewohnheiten in ihrem Leben einzuführen. Dies geht am besten spielerisch.
            <br /><br />
            Finden Sie passende Gewohnheiten unter Quests und bearbeiten Sie diese unter Todos. Wenn Sie eine Quest gefunden haben,
            wird diese für Sie vorerst angepinnt. Erst wenn Sie diese Starten, können Sie Belohnungen freischalten und aufsteigen.
            <br /><br />
            Falls sie noch keinen Erzfeind erwählt haben, empfehle ich dies zu tun, da dies die eigenen Anstrengungen erhögen kann.
            <br /><br />
            Viel Spaß.
          </Popup>
        </Menu.Item>
        <Menu.Item
          as="h2"
          style={headerHeadline}
          name="Gewohnheitstracker"
          onClick={headerClick}
          header
        />
        <Menu.Menu position="right">
          <Menu.Item>
            <Image
              src={translateProfileImageId(config.profileImageId)}
              size="mini"
            />
          </Menu.Item>
          <Menu.Item as="h4" name={user.data?.playerName} header />
          <Menu.Item header>
            <h4>{"LVL: " + user.data?.level}</h4>
          </Menu.Item>
          <Menu.Item style={headerLastMenu} header>
            <h4>{"Gold: " + user.data?.gold}</h4>
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    </Container>
  );
};
