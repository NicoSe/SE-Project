import { JwtHeader, JwtPayload } from "jwt-decode";

export interface ITokenData {
  headers: JwtHeader;
  payload: JwtPayload & ICustomPayload;
}

export interface ICustomPayload {
  username: string;
  id: number;
}
