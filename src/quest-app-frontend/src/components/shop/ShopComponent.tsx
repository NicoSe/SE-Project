import React, { useEffect } from "react";
import { toast } from "react-toastify";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { Container, Divider, Grid, Header, Image } from "semantic-ui-react";
import { MarketApi, UserApi } from "../../gen/api";
import { textShadow } from "../../global-styles";
import { divider } from "../../pages/styles";
import {
  backgroundImageShopItems,
  inventoryState,
  profileImageShopItems,
  userState,
} from "../../states";
import {
  GetAPIConfig,
  translateBackgroundImageId,
  translateProfileImageId,
} from "../helpers";
import { handleError } from "../utils/ErrorHandler";
import { shopContainer, shopItemGrid } from "./styles";

const ShopComponent: React.FC = () => {
  const bgItems = useRecoilValue(backgroundImageShopItems);
  const pgItems = useRecoilValue(profileImageShopItems);
  const setInventory = useSetRecoilState(inventoryState);
  const setUser = useSetRecoilState(userState);

  // fetch users to update money display
  async function fetchUser() {
    const userApi = new UserApi(GetAPIConfig());

    try {
      const res = await userApi.userGet();
      setUser({
        authenticated: true,
        data: res,
      });
    } catch (e) {
      handleError(e);
    }
  }

  // fetch users inventar to list it
  async function fetchInventory() {
    const marketApi = new MarketApi(GetAPIConfig());

    try {
      const res = await marketApi.marketGet();
      setInventory(res);
    } catch (e) {
      handleError(e);
    }
  }

  // buy a selected background image with a given price
  async function buyBackgroundImage(id: string, price: string) {
    const marketApi = new MarketApi(GetAPIConfig());

    try {
      await marketApi.marketPut(price, id);

      // update user and inventory
      fetchUser();
      fetchInventory();
      toast("Du hast erfolgreich ein Objekt im Shop gekauft.", {
        type: "success",
        position: "bottom-right",
      });
    } catch (e) {
      handleError(e, "shop");
    }
  }

  // buy a selected profile image with a given price
  async function buyProfileImage(id: string, price: string) {
    const marketApi = new MarketApi(GetAPIConfig());

    try {
      await marketApi.marketPut(price, undefined, id);

      // update user and inventory
      fetchUser();
      fetchInventory();
      toast("Du hast erfolgreich ein Objekt im Shop gekauft.", {
        type: "success",
        position: "bottom-right",
      });
    } catch (e) {
      handleError(e, "shop");
    }
  }

  useEffect(() => {
    fetchInventory();
  }, []);

  return (
    <Container style={shopContainer}>
      <Container>
        <Header style={{ textShadow: textShadow }} as="h2">
          Hintergründe:
        </Header>
        <Divider style={divider} />
        <Grid container columns={5} style={shopItemGrid}>
          {bgItems.length === 0 ? (
            <Header style={{ textShadow: textShadow }} as="h3">
              Du hast alle verfügbaren Objekte bereits im Inventar.
            </Header>
          ) : (
            bgItems.map((id) => {
              return (
                <Grid.Column key={id}>
                  <Image
                    label={{
                      as: "a",
                      color: "yellow",
                      content: "1000 Gold",
                      icon: "money",
                      ribbon: true,
                    }}
                    size="small"
                    src={translateBackgroundImageId(id)}
                    onClick={() => buyBackgroundImage(id, "1000")}
                  ></Image>
                </Grid.Column>
              );
            })
          )}
        </Grid>
      </Container>
      <Container>
        <Header style={{ textShadow: textShadow }} as="h2">
          Profilbilder:
        </Header>
        <Divider style={divider} />
        <Grid container columns={5} style={shopItemGrid}>
          {pgItems.length === 0 ? (
            <Header style={{ textShadow: textShadow }} as="h3">
              Du hast alle verfügbaren Objekte bereits im Inventar.
            </Header>
          ) : (
            pgItems.map((id) => {
              return (
                <Grid.Column key={id}>
                  <Image
                    label={{
                      as: "a",
                      color: "yellow",
                      content: "500 Gold",
                      icon: "money",
                      ribbon: true,
                    }}
                    size="small"
                    src={translateProfileImageId(id)}
                    onClick={() => buyProfileImage(id, "500")}
                  ></Image>
                </Grid.Column>
              );
            })
          )}
        </Grid>
      </Container>
    </Container>
  );
};

export default ShopComponent;
