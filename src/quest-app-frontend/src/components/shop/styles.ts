import { textShadow } from "../../global-styles";

//===============================================\\
//              ShopComponent.tsx               \\
//===============================================\\

export const shopContainer = {
  textAlign: "left",
  height: "56vh",
  maxHeight: "56vh",
  overflow: "auto",
  textShadow: textShadow,
};

export const shopItemGrid = {
  marginBottom: "2vh",
};
