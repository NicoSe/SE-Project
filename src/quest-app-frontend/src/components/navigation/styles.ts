import { fontFamily } from "../../global-styles";

//===============================================\\
//                Navigation.tsx                 \\
//===============================================\\

export const navigationButton = {
  fontSize: "2.3vmin",
  color: "rgba(0,0,0,.87)",
  background: "rgb(255,255,255)",
  padding: "1em 1.7em",
  margin: 0,
  marginTop: "15vh",
  textAlign: "right",
  verticalAlign: "middle",
  justifyContent: "center",
  borderRadius: "15px",
  fontWeight: 700,
  fontFamily: fontFamily,
};

export const navigationLink = {
  zIndex: 1,
  fontSize: "2.3vmin",
  color: "rgba(0,0,0,.87)",
  padding: "1em 1.7em",
  borderRadius: "15px",
  fontFamily: fontFamily,
};

export const navigationContainer = {
  height: "58vh",
  paddingRight: "1vh",
};
