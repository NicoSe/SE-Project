import { useLocation, useNavigate } from "@reach/router";
import React, { useEffect } from "react";
import { useResetRecoilState } from "recoil";
import { Button, Container, Divider, Step } from "semantic-ui-react";
import { userState } from "../../states";
import { logout } from "../helpers";
import { NavLink } from "./NavLink";
import { navigationButton, navigationContainer } from "./styles";

// navigation of the application, manages the URI
const MainNavigation: React.FC = () => {
  const resetUser = useResetRecoilState(userState);

  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location.pathname]);

  const logoutClick = () => {
    logout();
    resetUser();
    navigate("/login");
  };

  return (
    <Container style={navigationContainer}>
      <Step.Group style={{ border: "none" }} vertical fluid>
        <NavLink name="Todos" to="/todos" />
        <Divider hidden fitted />
        <NavLink name="Quests" to="/quests" />
        <Divider hidden fitted />
        <NavLink name="Profil" to="/profile" />
        <Divider hidden fitted />
        <NavLink name="Shop" to="/shop" />
        <Divider hidden fitted />
        <NavLink name="Inventar" to="/inventory" />
        <Divider hidden fitted />
        <Button style={navigationButton} onClick={logoutClick}>
          Abmelden
        </Button>
      </Step.Group>
    </Container>
  );
};

export default MainNavigation;

/**
 * <Container style={navigationContainer}>
            <Sticky>
                <List animated relaxed>
                    <List.Item>
                        <List.Content floated="left">
                            <NavLink name="Todos" to="/todos" />
                        </List.Content>
                    </List.Item>
                    <List.Item>
                        <List.Content floated="left">
                            <NavLink name="Quests" to="/quests" />
                        </List.Content>
                    </List.Item>
                    <List.Item>
                        <List.Content floated="left">
                            <NavLink name="Profil" to="/profile" />
                        </List.Content>
                    </List.Item>
                    <List.Item>
                        <List.Content floated="left">
                            <NavLink name="Shop" to="/shop" />
                        </List.Content>
                    </List.Item>
                    <List.Item>
                        <List.Content floated="left">
                            <Button classname="navLinkBtn" onClick={logoutClick}>Abmelden</Button>
                        </List.Content>
                    </List.Item>
                </List>
            </Sticky>
        </Container>
 */
