import { Link, useLocation } from "@reach/router";
import React from "react";
import { Step } from "semantic-ui-react";
import { navigationLink } from "./styles";

export interface INavigationLinkProps {
  to: string;
  name: string;
}

// component that highlight the active navigation item based on URI
export const NavLink: React.FC<INavigationLinkProps> = (props) => {
  var active = false;
  const location = useLocation().pathname;
  if (props.to === location) {
    active = true;
  }

  return (
    <Link to={props.to}>
      <Step style={navigationLink} active={active}>
        <Step.Title>{props.name}</Step.Title>
      </Step>
    </Link>
  );
};
