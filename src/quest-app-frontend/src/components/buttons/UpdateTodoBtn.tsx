import { useSetRecoilState } from "recoil";
import { Popup, Button } from "semantic-ui-react";
import { QuestApi } from "../../gen/api";
import { TodoListItem } from "../../gen/api/models/TodoListItem";
import { popupActiveState, popupContentState } from "../../states";
import { GetAPIConfig } from "../helpers";
import { handleError } from "../utils/ErrorHandler";

interface IUpdateTodoProps {
  updateTodoList: () => void;
  updateUser: () => void;
  todo: TodoListItem;
}

// component for a button that can update the work on a given todo and get reward
const UpdateTodoBtn: React.FC<IUpdateTodoProps> = (props) => {
  const setPopupActive = useSetRecoilState(popupActiveState);
  const setPopupConent = useSetRecoilState(popupContentState);

  // call endpoint to update a quest
  const updateTodo = async (id: string) => {
    const questApi = new QuestApi(GetAPIConfig());

    try {
      const reward = await questApi.questsTodosTodoIdFinishPut(id);
      props.updateUser();
      props.updateTodoList();
      setPopupConent(reward);
      setPopupActive(true);
    } catch (e) {
      handleError(e, "reward");
      setPopupActive(false);
    }
  };

  return (
    <Popup
      trigger={
        <Button
          size="small"
          color="green"
          icon="money"
          disabled={
            Date.parse(
              props.todo.availableClaim !== undefined
                ? props.todo.availableClaim
                : ""
            ) > Date.now()
          }
          style={{ marginTop: "8px" }}
          onClick={() => updateTodo(props.todo.id)}
        />
      }
      position="bottom left"
    >
      Belohnung für das Erfüllen dieses Todos einfordern.
    </Popup>
  );
};

export default UpdateTodoBtn;
