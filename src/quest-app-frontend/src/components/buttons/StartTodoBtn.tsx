import { useSetRecoilState } from "recoil";
import { Popup, Button } from "semantic-ui-react";
import { FrequencyEnum, QuestApi } from "../../gen/api";
import { TodoListItem } from "../../gen/api/models/TodoListItem";
import { popupActiveState } from "../../states";
import { GetAPIConfig } from "../helpers";
import { handleError } from "../utils/ErrorHandler";

interface IStartTodoProps {
  updateTodoList: () => void;
  frequency: FrequencyEnum;
  todo: TodoListItem;
}

// component for a button that can start a given todo
const StartTodoBtn: React.FC<IStartTodoProps> = (props) => {
  const setPopupActive = useSetRecoilState(popupActiveState);

  // call endpoint to start a quest
  const startTodo = async (id: string) => {
    const questApi = new QuestApi(GetAPIConfig());

    try {
      await questApi.questsTodosTodoIdStartPut(id, props.frequency);
      props.updateTodoList();
      setPopupActive(false);
    } catch (e) {
      handleError(e);
      setPopupActive(false);
    }
  };

  return (
    <Popup
      trigger={
        <Button
          size="small"
          color="green"
          icon="plus"
          style={{ marginTop: "8px" }}
          onClick={() => startTodo(props.todo.id)}
        />
      }
      position="bottom left"
    >
      Diese Quest als aktives Todo aufnehmen. Dabei wird automatisch "täglich"
      als Wiederholung eingestellt. Um dies zubearbeiten, klicken Sie auf die
      Quest.
    </Popup>
  );
};

export default StartTodoBtn;
