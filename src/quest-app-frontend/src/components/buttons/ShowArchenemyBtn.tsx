import { useSetRecoilState } from "recoil";
import { Button } from "semantic-ui-react";
import { UserApi } from "../../gen/api";
import { popupActiveState, popupContentState } from "../../states";
import { GetAPIConfig } from "../helpers";
import { handleError } from "../utils/ErrorHandler";

interface IShowArchenemyProps {
  archenemy: string;
}

const ShowArchenemyBtn: React.FC<IShowArchenemyProps> = (props) => {
  const setPopupActive = useSetRecoilState(popupActiveState);
  const setPopupContent = useSetRecoilState(popupContentState);

  const showArchenemyDetails = async () => {
    const userApi = new UserApi(GetAPIConfig());

    try {
      const todoDetails = await userApi.userByNamePlayerNameGet(
        props.archenemy
      );
      setPopupContent(todoDetails);
      setPopupActive(true);
    } catch (e) {
      handleError(e, "archenemy");
      setPopupActive(false);
    }
  };

  if (props.archenemy !== "")
    return (
      <Button icon="search" onClick={() => showArchenemyDetails()}></Button>
    );
  else return null;
};

export default ShowArchenemyBtn;
