import { useSetRecoilState } from "recoil";
import { Popup, Button } from "semantic-ui-react";
import { QuestApi, QuestListItem } from "../../gen/api";
import { popupActiveState } from "../../states";
import { GetAPIConfig } from "../helpers";
import { handleError } from "../utils/ErrorHandler";

interface IAddQuestProps {
  updateQuestList: () => void;
  quest: QuestListItem;
}

// component for a button that can add a quest to thr planned todo list
const AddQuestBtn: React.FC<IAddQuestProps> = (props) => {
  const setPopupActive = useSetRecoilState(popupActiveState);

  // call endpoint to add a quest to planned todos
  const takeQuest = async (id: string) => {
    const questApi = new QuestApi(GetAPIConfig());

    try {
      await questApi.questsTodosAddQuestIdPost(id);
      props.updateQuestList();
      setPopupActive(false);
    } catch (e) {
      handleError(e, "addQuest");
      setPopupActive(false);
    }
  };

  return (
    <Popup
      trigger={
        <Button
          size="small"
          style={{ marginTop: "8px" }}
          color="green"
          icon="pin"
          onClick={() => takeQuest(props.quest.id)}
        />
      }
      position="bottom center"
    >
      Diese Quest anpinnen, um sie unter Todos starten zu können.
    </Popup>
  );
};

export default AddQuestBtn;
