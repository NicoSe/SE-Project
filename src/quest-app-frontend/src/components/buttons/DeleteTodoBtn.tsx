import { useSetRecoilState } from "recoil";
import { Popup, Button } from "semantic-ui-react";
import { QuestApi } from "../../gen/api";
import { TodoListItem } from "../../gen/api/models/TodoListItem";
import { popupActiveState } from "../../states";
import { GetAPIConfig } from "../helpers";
import { handleError } from "../utils/ErrorHandler";

interface IDeleteTodoProps {
  updateTodoList: () => void;
  todo: TodoListItem;
}

// component of a button that can delete a given todo
const DeleteTodoBtn: React.FC<IDeleteTodoProps> = (props) => {
  const setPopupActive = useSetRecoilState(popupActiveState);

  // call endpoint to delete todo
  const deleteTodo = async (id: string) => {
    const questApi = new QuestApi(GetAPIConfig());

    try {
      await questApi.questsTodosTodoIdDelete(id);
      props.updateTodoList();
      setPopupActive(false);
    } catch (e) {
      handleError(e);
      setPopupActive(false);
    }
  };

  return (
    <Popup
      trigger={
        <Button
          size="small"
          color="red"
          icon="trash"
          style={{ marginTop: "8px" }}
          onClick={() => deleteTodo(props.todo.id)}
        />
      }
      position="bottom left"
    >
      Dieses Quest löschen.
    </Popup>
  );
};

export default DeleteTodoBtn;
