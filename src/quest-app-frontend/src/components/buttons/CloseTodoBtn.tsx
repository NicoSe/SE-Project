import { useSetRecoilState } from "recoil";
import { Popup, Button } from "semantic-ui-react";
import { QuestApi, Todo } from "../../gen/api";
import { TodoListItem } from "../../gen/api/models/TodoListItem";
import { popupActiveState } from "../../states";
import { GetAPIConfig } from "../helpers";
import { handleError } from "../utils/ErrorHandler";

interface ICloseTodoProps {
  updateTodoList: () => void;
  todo: TodoListItem;
}

// component for a button that can close/end the work on a given todo
const CloseTodoBtn: React.FC<ICloseTodoProps> = (props) => {
  const setPopupActive = useSetRecoilState(popupActiveState);

  // call endpoint to close a quest
  const closeTodo = async (id: string) => {
    const questApi = new QuestApi(GetAPIConfig());

    try {
      await questApi.questsTodosTodoIdClosePut(id);
      props.updateTodoList();
      setPopupActive(false);
    } catch (e) {
      handleError(e);
      setPopupActive(false);
    }
  };

  return (
    <Popup
      trigger={
        <Button
          size="small"
          color="green"
          icon="check"
          style={{ marginTop: "8px" }}
          onClick={() => closeTodo(props.todo.id)}
        />
      }
      position="bottom left"
    >
      Dieses Todo auf erledigt schieben. Dabei wird keine Belohnung ausgezahlt!
    </Popup>
  );
};

export default CloseTodoBtn;
