import bgId1 from "../assets/backgroundImages/id1.jpg";
import bgId2 from "../assets/backgroundImages/id2.jpg";
import pgId1 from "../assets/profileImages/id1.png";
import pgId2 from "../assets/profileImages/id2.png";
import pgId3 from "../assets/profileImages/id3.png";
import pgId4 from "../assets/profileImages/id4.png";
import pgId5 from "../assets/profileImages/id5.png";
import pgId6 from "../assets/profileImages/id6.png";
import {
  AuthorizationApi,
  Configuration,
  createConfiguration,
} from "../gen/api";
import { handleError } from "./utils/ErrorHandler";

export const ACCESS_TOKEN_KEY = "access_token";

// pull configuration from session storage
export const GetAPIConfig = (): Configuration => {
  return createConfiguration({
    authMethods: {
      bearerAuth: {
        tokenProvider: {
          getToken: () => localStorage.getItem(ACCESS_TOKEN_KEY) ?? "",
        },
      },
    },
  });
};

export const login = async (email: string, password: string) => {
  // login user with given email and password
  const authApi = new AuthorizationApi(GetAPIConfig());
  try {
    const response = await authApi.authLoginPost({
      email,
      password,
    });
    localStorage.setItem(
      ACCESS_TOKEN_KEY,
      response.token !== undefined ? response.token : ""
    );
  } catch (e) {
    handleError(e, "auth");
    throw new Error();
  }
};

export const logout = () => {
  localStorage.removeItem(ACCESS_TOKEN_KEY);
};

// return import of background image by id
export const translateBackgroundImageId = (id: string) => {
  switch (id) {
    case "0":
      return bgId1;
    case "1":
      return bgId2;
    default:
      return null;
  }
};

// return import of profile image by id
export const translateProfileImageId = (id: string) => {
  switch (id) {
    case "0":
      return pgId1;
    case "1":
      return pgId2;
    case "2":
      return pgId3;
    case "3":
      return pgId4;
    case "4":
      return pgId5;
    case "5":
      return pgId6;
    default:
      return null;
  }
};
