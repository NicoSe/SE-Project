import { textShadow } from "../../global-styles";

//===============================================\\
//              InventatarComponent.tsx               \\
//===============================================\\

export const inventarContainer = {
  textAlign: "left",
  height: "56vh",
  maxHeight: "56vh",
  overflow: "auto",
  textShadow: textShadow,
};

export const inventarItemGrid = {
  marginBottom: "2vh",
};
