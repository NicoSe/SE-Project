import React, { useEffect } from "react";
import { toast } from "react-toastify";
import { useRecoilState } from "recoil";
import { Container, Divider, Grid, Header, Image } from "semantic-ui-react";
import { MarketApi, UserApi } from "../../gen/api";
import { textShadow } from "../../global-styles";
import { divider } from "../../pages/styles";
import { configState, inventoryState } from "../../states";
import {
  GetAPIConfig,
  translateBackgroundImageId,
  translateProfileImageId,
} from "../helpers";
import { handleError } from "../utils/ErrorHandler";
import { inventarContainer, inventarItemGrid } from "./styles";

const InventoryComponent: React.FC = () => {
  const [config, setConfig] = useRecoilState(configState);
  const [inventory, setInventory] = useRecoilState(inventoryState);

  // fetch users inventar to list it
  async function fetchInventory() {
    const marketApi = new MarketApi(GetAPIConfig());

    try {
      const res = await marketApi.marketGet();
      setInventory(res);
    } catch (e) {
      handleError(e);
    }
  }

  // set backgroundImageId but dont touch profileImageId
  async function setBackgroundImageId(id: string) {
    const userApi = new UserApi(GetAPIConfig());

    try {
      await userApi.userConfigPut({
        backgroundImageId: id,
        profileImageId: config.profileImageId,
      });

      // update user config
      const res = await userApi.userConfigGet();
      setConfig(res);
      toast("Du hast erfolgreich das Hintergrundbild geändert.", {
        type: "success",
        position: "bottom-right",
      });
    } catch (e) {
      handleError(e);
    }
  }

  // set profileImageId but dont touch backgroundImageId
  async function setProfileImageId(id: string) {
    const userApi = new UserApi(GetAPIConfig());

    try {
      await userApi.userConfigPut({
        backgroundImageId: config.backgroundImageId,
        profileImageId: id,
      });

      // update user config
      const res = await userApi.userConfigGet();
      setConfig(res);
      toast("Du hast erfolgreich dein Profilbild geändert.", {
        type: "success",
        position: "bottom-right",
      });
    } catch (e) {
      handleError(e);
    }
  }

  useEffect(() => {
    fetchInventory();
  }, []);

  return (
    <Container style={inventarContainer}>
      <Container>
        <Header style={{ textShadow: textShadow }} as="h2">
          Hintergründe im Besitz:
        </Header>
        <Divider style={divider} />
        <Grid container columns={5} style={inventarItemGrid}>
          {"0" === config.backgroundImageId ? (
            <Grid.Column key="0">
              <Image
                label={{
                  as: "a",
                  color: "green",
                  content: "ausgewählt",
                  icon: "check",
                  ribbon: true,
                }}
                size="small"
                src={translateBackgroundImageId("0")}
              ></Image>
            </Grid.Column>
          ) : (
            <Grid.Column key="0">
              <Image
                src={translateBackgroundImageId("0")}
                size="small"
                onClick={() => setBackgroundImageId("0")}
              ></Image>
            </Grid.Column>
          )}
          {inventory.backgroundImageInventar.map((id) => {
            if (id === "0") return;
            if (id === config.backgroundImageId) {
              return (
                <Grid.Column key={id}>
                  <Image
                    label={{
                      as: "a",
                      color: "green",
                      content: "ausgewählt",
                      icon: "check",
                      ribbon: true,
                    }}
                    size="small"
                    src={translateBackgroundImageId(id)}
                  ></Image>
                </Grid.Column>
              );
            } else {
              return (
                <Grid.Column key={id}>
                  <Image
                    src={translateBackgroundImageId(id)}
                    size="small"
                    onClick={() => setBackgroundImageId(id)}
                  ></Image>
                </Grid.Column>
              );
            }
          })}
        </Grid>
      </Container>
      <Container>
        <Header style={{ textShadow: textShadow }} as="h2">
          Profilbilder im Besitz:
        </Header>
        <Divider style={divider} />
        <Grid container columns={5} style={inventarItemGrid}>
          {"0" === config.profileImageId ? (
            <Grid.Column key="0">
              <Image
                label={{
                  as: "a",
                  color: "green",
                  content: "ausgewählt",
                  icon: "check",
                  ribbon: true,
                }}
                size="small"
                src={translateProfileImageId("0")}
              ></Image>
            </Grid.Column>
          ) : (
            <Grid.Column key="0">
              <Image
                src={translateProfileImageId("0")}
                size="small"
                onClick={() => setProfileImageId("0")}
              ></Image>
            </Grid.Column>
          )}
          {inventory.profileImageInventar.map((id) => {
            if (id === "0") return;
            if (id === config.profileImageId) {
              return (
                <Grid.Column key={id}>
                  <Image
                    label={{
                      as: "a",
                      color: "green",
                      content: "ausgewählt",
                      icon: "check",
                      ribbon: true,
                    }}
                    size="small"
                    src={translateProfileImageId(id)}
                  ></Image>
                </Grid.Column>
              );
            } else {
              return (
                <Grid.Column key={id}>
                  <Image
                    src={translateProfileImageId(id)}
                    size="small"
                    onClick={() => setProfileImageId(id)}
                  ></Image>
                </Grid.Column>
              );
            }
          })}
        </Grid>
      </Container>
    </Container>
  );
};

export default InventoryComponent;
