import React from "react";
import { useSetRecoilState } from "recoil";
import { Container, Divider, Grid, Header, List } from "semantic-ui-react";
import { QuestApi } from "../../gen/api";
import { popupActiveState, popupContentState } from "../../states";
import { GetAPIConfig } from "../helpers";
import { divider } from "../../pages/styles";
import { handleError } from "../utils/ErrorHandler";
import { todoItemContainer, todoItemGrid, todoItemHeader } from "./styles";
import CloseTodoBtn from "../buttons/CloseTodoBtn";
import DeleteTodoBtn from "../buttons/DeleteTodoBtn";
import UpdateTodoBtn from "../buttons/UpdateTodoBtn";
import { TodoListItem } from "../../gen/api/models/TodoListItem";
import { textShadow } from "../../global-styles";

export interface IActiveTodoItemProps {
  updateUser: () => void;
  updateTodoList: () => void;
  todos: TodoListItem[];
}

// component to display active todos
const ActiveTodoItem: React.FC<IActiveTodoItemProps> = (props) => {
  const setPopupActive = useSetRecoilState(popupActiveState);
  const setPopupContent = useSetRecoilState(popupContentState);

  // open popup with todos content
  const showTodoDetails = async (id: string) => {
    const questApi = new QuestApi(GetAPIConfig());
    try {
      const todoDetails = await questApi.questsTodosTodoIdGet(id);
      setPopupContent(todoDetails);
      setPopupActive(true);
    } catch (e) {
      handleError(e);
    }
  };

  return (
    <Container>
      <Header style={{ textShadow: textShadow }} as="h2">
        Aktive Todos:
      </Header>
      <Divider style={divider} />
      <Grid container columns={5} style={todoItemGrid}>
        {props.todos.length === 0 ? (
          <Grid.Column width={16}>
            <Header style={{ textShadow: textShadow }} as="h3">
              Momentan sind keine Quests aktiv.
            </Header>
          </Grid.Column>
        ) : (
          props.todos.map((todo) => {
            return (
              <Grid.Column>
                <Container style={todoItemContainer}>
                  <Container
                    style={{
                      display: "inline-block",
                      height: "12.2vh",
                      maxHeight: "12.2vh",
                      overflow: "hidden",
                    }}
                  >
                    <Header
                      className="popupLink"
                      as="h3"
                      style={todoItemHeader}
                      onClick={() => showTodoDetails(todo.id)}
                    >
                      {todo.title}
                    </Header>
                    <List
                      className="popupLink"
                      style={{ marginTop: "0", fontSize: "6px" }}
                      onClick={() => showTodoDetails(todo.id)}
                    >
                      <List.Item>_ ____ _ __ __ ___ _ __ _ ____ _ __</List.Item>
                      <List.Item>___ __ __ _ _ ____ _ __ _ _ __</List.Item>
                      <List.Item>_ ____ _ __ _ __ __ ___ _</List.Item>
                      <List.Item>
                        __ ___ _ __ __ ___ _ _ __ __ ___ _ _ __
                      </List.Item>
                      <List.Item>___ __ __ _ _ _ __</List.Item>
                      <List.Item>_ _ ___ __ _ _ ____ _ __</List.Item>
                      <List.Item> </List.Item>
                      <List.Item> </List.Item>
                      <List.Item> </List.Item>
                      <List.Item>_____ __ __ __ _ _</List.Item>
                    </List>
                  </Container>
                  <UpdateTodoBtn
                    todo={todo}
                    updateUser={() => props.updateUser()}
                    updateTodoList={() => props.updateTodoList()}
                  />
                  <CloseTodoBtn
                    todo={todo}
                    updateTodoList={() => props.updateTodoList()}
                  />
                  <DeleteTodoBtn
                    todo={todo}
                    updateTodoList={() => props.updateTodoList()}
                  />
                </Container>
              </Grid.Column>
            );
          })
        )}
      </Grid>
    </Container>
  );
};

export default ActiveTodoItem;
