import questBg from "../../assets/quest.png";
import questFinishedBg from "../../assets/questFinished.png";
import { textShadow } from "../../global-styles";

//===============================================\\
//              TodosComponent.tsx               \\
//===============================================\\

export const todosContainer = {
  textAlign: "left",
  height: "56vh",
  maxHeight: "56vh",
  overflow: "auto",
  textShadow: textShadow,
};

export const todoItemGrid = {
  overflow: "auto",
};

export const todoItemContainer = {
  height: "20vh",
  textAlign: "center",
  marginBottom: "1vh",
  backgroundImage: `url(${questBg})`,
  backgroundSize: "contain",
  backgroundPosition: "center center",
  backgroundRepeat: "no-repeat",
  textShadow: textShadow,
};

export const todoItemFinishedContainer = {
  height: "8vh",
  textAlign: "center",
  marginBottom: "1vh",
  backgroundImage: `url(${questFinishedBg})`,
  backgroundSize: "contain",
  backgroundPosition: "center center",
  backgroundRepeat: "no-repeat",
  textShadow: textShadow,
};

export const todoItemHeader = {
  paddingTop: "3vh",
  textShadow: textShadow,
  marginBottom: 0,
};
