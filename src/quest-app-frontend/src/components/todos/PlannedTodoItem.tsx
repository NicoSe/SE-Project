import React from "react";
import { useSetRecoilState } from "recoil";
import { Container, Divider, Grid, Header, List } from "semantic-ui-react";
import { handleError } from "../../components/utils/ErrorHandler";
import { QuestApi } from "../../gen/api";
import { popupActiveState, popupContentState } from "../../states";
import { GetAPIConfig } from "../helpers";
import { divider } from "../../pages/styles";
import { todoItemContainer, todoItemGrid, todoItemHeader } from "./styles";
import DeleteTodoBtn from "../buttons/DeleteTodoBtn";
import StartTodoBtn from "../buttons/StartTodoBtn";
import { TodoListItem } from "../../gen/api/models/TodoListItem";
import { textShadow } from "../../global-styles";

export interface IPlannedTodoItemProps {
  updateTodoList: () => void;
  todos: TodoListItem[];
}

// component to display planned todos
const PlannedTodoItem: React.FC<IPlannedTodoItemProps> = (props) => {
  const setPopupActive = useSetRecoilState(popupActiveState);
  const setPopupContent = useSetRecoilState(popupContentState);

  // open popup with todos content
  const showTodoDetails = async (id: string) => {
    const questApi = new QuestApi(GetAPIConfig());
    try {
      const todoDetails = await questApi.questsTodosTodoIdGet(id);
      setPopupContent(todoDetails);
      setPopupActive(true);
    } catch (e) {
      handleError(e);
    }
  };

  return (
    <Container>
      <Header as="h2" style={{ marginTop: "2vh", textShadow: textShadow }}>
        Geplante Todos:
      </Header>
      <Divider style={divider} />
      <Grid container columns={5} style={todoItemGrid}>
        {props.todos.length === 0 ? (
          <Grid.Column width={16}>
            <Header style={{ textShadow: textShadow }} as="h3">
              Momentan sind keine Quests angepinnt. Gehe zum Bereich Quests um Todos zu finden.
            </Header>
          </Grid.Column>
        ) : (
          props.todos.map((todo) => {
            return (
              <Grid.Column>
                <Container style={todoItemContainer}>
                  <Container
                    style={{
                      display: "inline-block",
                      height: "12.2vh",
                      maxHeight: "12.2vh",
                      overflow: "hidden",
                    }}
                  >
                    <Header
                      className="popupLink"
                      as="h3"
                      style={todoItemHeader}
                      onClick={() => showTodoDetails(todo.id)}
                    >
                      {todo.title}
                    </Header>
                    <List
                      className="popupLink"
                      style={{ marginTop: "0", fontSize: "6px" }}
                      onClick={() => showTodoDetails(todo.id)}
                    >
                      <List.Item>_ ____ _ __ __ ___ _ __ _ ____ _ __</List.Item>
                      <List.Item>___ __ __ _ _ ____ _ __ _ _ __</List.Item>
                      <List.Item>_ ____ _ __ _ __ __ ___ _</List.Item>
                      <List.Item>
                        __ ___ _ __ __ ___ _ _ __ __ ___ _ _ __
                      </List.Item>
                      <List.Item>___ __ __ _ _ _ __</List.Item>
                      <List.Item>_ _ ___ __ _ _ ____ _ __</List.Item>
                      <List.Item> </List.Item>
                      <List.Item> </List.Item>
                      <List.Item> </List.Item>
                      <List.Item>_____ __ __ __ _ _</List.Item>
                    </List>
                  </Container>
                  <StartTodoBtn
                    todo={todo}
                    updateTodoList={() => props.updateTodoList()}
                    frequency="DAILY"
                  />
                  <DeleteTodoBtn
                    todo={todo}
                    updateTodoList={() => props.updateTodoList()}
                  />
                </Container>
              </Grid.Column>
            );
          })
        )}
      </Grid>
    </Container>
  );
};

export default PlannedTodoItem;
