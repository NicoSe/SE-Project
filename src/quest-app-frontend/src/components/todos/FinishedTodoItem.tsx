import React from "react";
import { useSetRecoilState } from "recoil";
import { Container, Divider, Grid, Header } from "semantic-ui-react";
import { handleError } from "../../components/utils/ErrorHandler";
import { QuestApi } from "../../gen/api";
import { TodoListItem } from "../../gen/api/models/TodoListItem";
import { textShadow } from "../../global-styles";
import { divider } from "../../pages/styles";
import { popupActiveState, popupContentState } from "../../states";
import { GetAPIConfig } from "../helpers";
import {
  todoItemFinishedContainer,
  todoItemGrid,
  todoItemHeader,
} from "./styles";

export interface IFinishedTodoItemProps {
  todos: TodoListItem[];
}

// component to display finished todos
const FinishedTodoItem: React.FC<IFinishedTodoItemProps> = (props) => {
  const setPopupActive = useSetRecoilState(popupActiveState);
  const setPopupContent = useSetRecoilState(popupContentState);

  // open popup with todos content
  const showTodoDetails = async (id: string) => {
    const questApi = new QuestApi(GetAPIConfig());
    try {
      const todoDetails = await questApi.questsTodosTodoIdGet(id);
      setPopupContent(todoDetails);
      setPopupActive(true);
    } catch (e) {
      handleError(e);
    }
  };

  return (
    <Container>
      <Header as="h2" style={{ marginTop: "2vh", textShadow: textShadow }}>
        Abgeschlossene Todos:
        <Header.Subheader>
          Zuletzt abgeschlossene Quests werden zuerst angezeigt.
        </Header.Subheader>
      </Header>
      <Divider style={divider} />
      <Grid container columns={5} style={todoItemGrid}>
        {props.todos.length === 0 ? (
          <Grid.Column width={16}>
            <Header style={{ textShadow: textShadow }} as="h3">
              Noch keine abgeschlossenen Todos vorhanden.
            </Header>
          </Grid.Column>
        ) : (
          props.todos.map((todo) => {
            return (
              <Grid.Column>
                <Container style={todoItemFinishedContainer}>
                  <Header
                    className="popupLink"
                    as="h3"
                    style={todoItemHeader}
                    onClick={() => showTodoDetails(todo.id)}
                  >
                    {todo.title}
                  </Header>
                </Container>
              </Grid.Column>
            );
          })
        )}
      </Grid>
    </Container>
  );
};

export default FinishedTodoItem;
