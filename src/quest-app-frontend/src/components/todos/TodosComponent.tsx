import React, { useEffect } from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { Container } from "semantic-ui-react";
import { handleError } from "../../components/utils/ErrorHandler";
import { QuestApi, UserApi } from "../../gen/api";
import {
  activeTodosState,
  finishedTodosState,
  plannedTodosState,
  rankingState,
  todosState,
  userState,
} from "../../states";
import { GetAPIConfig } from "../helpers";
import ActiveTodoItem from "./ActiveTodoItem";
import FinishedTodoItem from "./FinishedTodoItem";
import PlannedTodoItem from "./PlannedTodoItem";
import { todosContainer } from "./styles";

const TodosComponent: React.FC = () => {
  const setUser = useSetRecoilState(userState);
  const setRanking = useSetRecoilState(rankingState);
  const setTodos = useSetRecoilState(todosState);
  const activeTodos = useRecoilValue(activeTodosState);
  const plannedTodos = useRecoilValue(plannedTodosState);
  const finishedTodos = useRecoilValue(finishedTodosState);

  // update user and rannking when getting rewards
  const fetchUserAndRanking = async () => {
    const userApi = new UserApi(GetAPIConfig());
    try {
      const res = await userApi.userGet();
      setUser({
        authenticated: true,
        data: res,
      });
      const ranking = await userApi.userRanksGet();
      setRanking(ranking);
    } catch (e) {
      handleError(e);
    }
  };

  // request todo list from server
  const fetchTodoList = async () => {
    const questApi = new QuestApi(GetAPIConfig());

    try {
      const todos = await questApi.questsTodosGet();
      setTodos(todos);
    } catch (e) {
      handleError(e);
    }
  };

  useEffect(() => {
    fetchTodoList();
  }, []);

  return (
    <Container style={todosContainer}>
      <ActiveTodoItem
        updateUser={() => fetchUserAndRanking()}
        updateTodoList={() => fetchTodoList()}
        todos={activeTodos}
      />

      <PlannedTodoItem
        updateTodoList={() => fetchTodoList()}
        todos={plannedTodos}
      />

      <FinishedTodoItem todos={finishedTodos} />
    </Container>
  );
};

export default TodosComponent;
