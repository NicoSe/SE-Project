import React from "react";
import { useSetRecoilState } from "recoil";
import { popupActiveState, popupContentState } from "../../states";
import { Container, Header, Grid, List } from "semantic-ui-react";
import { QuestApi, QuestListItem } from "../../gen/api";
import { GetAPIConfig } from "../helpers";
import { handleError } from "../utils/ErrorHandler";
import {
  questItemContainer,
  questItemGrid,
  questItemGridContainer,
  questItemHeader,
} from "./styles";
import AddQuestBtn from "../buttons/AddQuestBtn";

export interface IQuestItemProps {
  updateQuestList: () => void;
  quests: QuestListItem[];
}

const QuestItem: React.FC<IQuestItemProps> = (props) => {
  const setPopupActive = useSetRecoilState(popupActiveState);
  const setPopupContent = useSetRecoilState(popupContentState);

  // open popup with quests content
  const showQuestDetails = async (id: string) => {
    const questApi = new QuestApi(GetAPIConfig());
    try {
      const questDetails = await questApi.questsQuestIdGet(id);
      setPopupContent(questDetails);
      setPopupActive(true);
    } catch (e) {
      handleError(e);
    }
  };

  return (
    <Container style={questItemGridContainer}>
      <Grid container columns={5} style={questItemGrid}>
        {props.quests.length === 0 ? (
          <Grid.Column width={16}>
            <Header as="h3">Es sind keine Quests verfügbar.</Header>
          </Grid.Column>
        ) : (
          props.quests.map((quest) => {
            return (
              <Grid.Column>
                <Container style={questItemContainer}>
                  <Container
                    style={{
                      display: "inline-block",
                      height: "12.2vh",
                      maxHeight: "12.2vh",
                      overflow: "hidden",
                    }}
                  >
                    <Header
                      className="popupLink"
                      as="h3"
                      style={questItemHeader}
                      onClick={() => showQuestDetails(quest.id)}
                    >
                      {quest.title}
                    </Header>
                    <List
                      className="popupLink"
                      style={{ marginTop: "0", fontSize: "6px" }}
                      onClick={() => showQuestDetails(quest.id)}
                    >
                      <List.Item>_ ____ _ __ __ ___ _ __ _ ____ _ __</List.Item>
                      <List.Item>___ __ __ _ _ ____ _ __ _ _ __</List.Item>
                      <List.Item>_ ____ _ __ _ __ __ ___ _</List.Item>
                      <List.Item>
                        __ ___ _ __ __ ___ _ _ __ __ ___ _ _ __
                      </List.Item>
                      <List.Item>___ __ __ _ _ _ __</List.Item>
                      <List.Item>_ _ ___ __ _ _ ____ _ __</List.Item>
                      <List.Item> </List.Item>
                      <List.Item> </List.Item>
                      <List.Item> </List.Item>
                      <List.Item>_____ __ __ __ _ _</List.Item>
                    </List>
                  </Container>
                  <AddQuestBtn
                    quest={quest}
                    updateQuestList={() => props.updateQuestList()}
                  />
                </Container>
              </Grid.Column>
            );
          })
        )}
      </Grid>
    </Container>
  );
};

export default QuestItem;
