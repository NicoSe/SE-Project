import React, { useEffect } from "react";
import {
  Container,
  Input,
  Header,
  List,
  Divider,
} from "semantic-ui-react";
import { QuestApi } from "../../gen/api";
import { GetAPIConfig } from "../helpers";
import { handleError } from "../../components/utils/ErrorHandler";

import QuestItem from "./QuestItem";
import { questContainer } from "./styles";
import { divider } from "../../pages/styles";
import { useRecoilState } from "recoil";
import { questsState } from "../../states";
import { textShadow } from "../../global-styles";

const QuestsComponent: React.FC = () => {
  const [quests, setQuests] = useRecoilState(questsState);

  // fetch available quests from server
  async function fetchQuestList(searchText: string) {
    const questApi = new QuestApi(GetAPIConfig());

    try {
      const quests = await questApi.questsGet(searchText);
      setQuests(quests);
    } catch (e) {
      handleError(e, "quest");
    }
  }

  useEffect(() => {
    fetchQuestList("");
  }, []);

  return (
    <Container style={questContainer}>
      <List>
        <List.Item>
          <List.Content>
            <Header
              as="h3"
              floated="left"
              style={{ marginTop: "6px", textShadow: textShadow }}
            >
              Suchbegriff:{" "}
            </Header>
            <Input
              onChange={(e, data) => {
                e.preventDefault();
                fetchQuestList(data.value);
              }}
            />
          </List.Content>
        </List.Item>
      </List>
      <Divider style={divider} />
      <QuestItem updateQuestList={() => fetchQuestList("")} quests={quests} />
    </Container>
  );
};

export default QuestsComponent;
