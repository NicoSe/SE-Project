import questBg from "../../assets/quest.png";

//===============================================\\
//                QuestItem.tsx                  \\
//===============================================\\

export const questItemGridContainer = {
  textAlign: "left",
};

export const questItemContainer = {
  height: "20vh",
  textAlign: "center",
  marginBottom: "1vh",
  backgroundImage: `url(${questBg})`,
  backgroundSize: "contain",
  backgroundPosition: "center center",
  backgroundRepeat: "no-repeat",
};

export const questItemGrid = {
  maxHeight: "54vh",
  overflow: "auto",
};

export const questItemHeader = {
  paddingTop: "3vh",
  marginBottom: 0,
};

//===============================================\\
//              QuestComponent.tsx               \\
//===============================================\\

export const questContainer = {
  textAlign: "left",
};
