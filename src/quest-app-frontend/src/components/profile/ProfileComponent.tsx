import { navigate } from "@reach/router";
import React, { MouseEvent, useState } from "react";
import { toast } from "react-toastify";
import { useRecoilState, useSetRecoilState } from "recoil";
import {
  Button,
  Container,
  Divider,
  Header,
  Input,
  List,
  Table,
  Image,
} from "semantic-ui-react";
import { RankUser, UserApi } from "../../gen/api";
import { configState, rankingState, userState } from "../../states";
import { GetAPIConfig, logout, translateProfileImageId } from "../helpers";
import { handleError } from "../utils/ErrorHandler";
import ShowArchenemyBtn from "../buttons/ShowArchenemyBtn";
import {
  profileContainer,
  profileHeader,
  profileHeaderHeading,
  profileTable,
} from "./styles";
import { divider } from "../../pages/styles";

const ProfileComponent: React.FC = () => {
  // manage state
  const [config] = useRecoilState(configState);
  const [user, setUser] = useRecoilState(userState);
  var userData = user.data;
  const [playerName, setPlayerName] = useState<string>(
    userData !== undefined ? userData.playerName : ""
  );
  const [archenemy, setArchenemy] = useState<string>(
    userData !== undefined
      ? userData.archenemy === undefined
        ? ""
        : userData.archenemy
      : ""
  );
  const [email, setEmail] = useState<string>(
    userData !== undefined ? userData.email : ""
  );
  const [password, setPassword] = useState<string>("");
  const [passwordAgain, setPasswordAgain] = useState<string>("");
  const [submitted, setSubmitted] = useState<boolean>(false);

  // indicate if any changes are made by user
  const [changed, setChanged] = useState<boolean>(false);

  // ranking state to update ranking after user change
  const setRankings = useSetRecoilState(rankingState);

  const userApi = new UserApi(GetAPIConfig());

  if (userData === undefined) {
    logout();
    navigate("/login");
    return <div></div>;
  }

  const saveBtn = async (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    // prevent user from double click
    if (submitted || !changed) {
      return;
    }
    setSubmitted(true);

    // validation
    var pn = undefined;
    var e = undefined;
    var pw = undefined;
    var ae = undefined;

    if (playerName === "") {
      toast("Spielername darf nicht leer sein!", {
        type: "error",
        position: "bottom-right",
      });
      setSubmitted(false);
      return;
    }
    if (email === "") {
      toast("Email darf nicht leer sein!", {
        type: "error",
        position: "bottom-right",
      });
      setSubmitted(false);
      return;
    }

    if (playerName !== userData?.playerName) {
      pn = playerName;
    }
    if (email !== userData?.email) {
      e = email;
    }
    if (archenemy !== userData?.archenemy) {
      ae = archenemy;
    }
    if (password !== "") {
      if (password !== passwordAgain) {
        toast("Passwörter stimmen nicht überein!", {
          type: "error",
          position: "bottom-right",
        });
        setSubmitted(false);
        return;
      }
      pw = password;
    }

    // update and user data
    try {
      await userApi.userPut({
        playerName: pn,
        email: e,
        password: pw,
        archenemy: ae,
      });
      const res = await userApi.userGet(GetAPIConfig());
      setUser({
        authenticated: true,
        data: res,
      });

      // update ranking
      fetchRankingList();

      // update reset states
      userData = res;
      setPlayerName(userData !== undefined ? userData.playerName : "");
      setArchenemy(
        userData !== undefined
          ? userData.archenemy === undefined
            ? ""
            : userData.archenemy
          : ""
      );
      setEmail(userData !== undefined ? userData.email : "");
      setPassword("");
      setPasswordAgain("");

      setSubmitted(false);
    } catch (e) {
      handleError(e);
      setSubmitted(false);
    }
  };

  const resetBtn = async (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    // prevent user from double click
    if (submitted || !changed) {
      return;
    }
    setSubmitted(true);

    setPlayerName(userData !== undefined ? userData.playerName : "");
    setArchenemy(
      userData !== undefined
        ? userData.archenemy === undefined
          ? ""
          : userData.archenemy
        : ""
    );
    setEmail(userData !== undefined ? userData.email : "");
    setPassword("");
    setPasswordAgain("");

    setSubmitted(false);
    setChanged(false);
  };

  async function fetchRankingList() {
    try {
      const ranks = await userApi.userRanksGet();
      if (ranks.users.length === 0) {
        const msg = new RankUser();
        msg.playerName = "Keine Daten verfügbar";
        ranks.users.concat(msg);
      }
      setRankings(ranks);
    } catch (e) {
      handleError(e);
    }
  }

  return (
    <Container style={profileContainer}>
      <List>
        <List.Item>
          <List.Content floated="right">
            <Header as="h3" style={profileHeaderHeading}>
              Profilbild:
            </Header>
            <br />
            <Image
              src={translateProfileImageId(config.profileImageId)}
              size="small"
            />
          </List.Content>
          <List.Content floated="left">
            <Table basic compact style={profileTable}>
              <Table.Body>
                <Table.Row>
                  <Table.Cell>
                    <Header style={profileHeaderHeading} as="h3">
                      Nutzerinformationen:
                    </Header>
                  </Table.Cell>
                  <Table.Cell></Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell collapsing>
                    <Header style={profileHeader} as="h4">
                      Spielername:{" "}
                    </Header>
                  </Table.Cell>
                  <Table.Cell>
                    <Input
                      iconPosition="left"
                      icon="user"
                      value={playerName}
                      loading={submitted}
                      disabled={submitted}
                      onChange={(e, data) => {
                        e.preventDefault();
                        setChanged(true);
                        setPlayerName(data.value);
                      }}
                    />
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <Header style={profileHeader} as="h4">
                      Email:{" "}
                    </Header>
                  </Table.Cell>
                  <Table.Cell>
                    <Input
                      iconPosition="left"
                      icon="at"
                      value={email}
                      loading={submitted}
                      disabled={submitted}
                      onChange={(e, data) => {
                        e.preventDefault();
                        setChanged(true);
                        setEmail(data.value);
                      }}
                    />
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell singleLine>
                    <Header style={profileHeader} as="h4">
                      Erzfeind-Spielername:{" "}
                    </Header>
                  </Table.Cell>
                  <Table.Cell>
                    <Input
                      iconPosition="left"
                      icon="address card"
                      value={archenemy}
                      loading={submitted}
                      disabled={submitted}
                      onChange={(e, data) => {
                        e.preventDefault();
                        setChanged(true);
                        setArchenemy(data.value);
                      }}
                    />
                    <ShowArchenemyBtn archenemy={archenemy} />
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <Header style={profileHeader} as="h4">
                      Password:{" "}
                    </Header>
                  </Table.Cell>
                  <Table.Cell>
                    <Input
                      iconPosition="left"
                      icon="key"
                      value={password}
                      loading={submitted}
                      disabled={submitted}
                      onChange={(e, data) => {
                        e.preventDefault();
                        setChanged(true);
                        setPassword(data.value);
                      }}
                    />
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell singleLine>
                    <Header style={profileHeader} as="h4">
                      Password wiederholen:{" "}
                    </Header>
                  </Table.Cell>
                  <Table.Cell>
                    <Input
                      iconPosition="left"
                      icon="key"
                      value={passwordAgain}
                      loading={submitted}
                      disabled={submitted}
                      onChange={(e, data) => {
                        e.preventDefault();
                        setChanged(true);
                        setPasswordAgain(data.value);
                      }}
                    />
                  </Table.Cell>
                </Table.Row>
              </Table.Body>
            </Table>
          </List.Content>
        </List.Item>
      </List>

      <List>
        <List.Item>
          <List.Content floated="left">
            <Header style={profileHeaderHeading} as="h3">
              Statik:
            </Header>
          </List.Content>
        </List.Item>
        <List.Item>
          <List.Content floated="left">
            <Header style={profileHeader} as="h4">
              Erfahrungspunkte: {userData.experience}
            </Header>
          </List.Content>
        </List.Item>
        <List.Item>
          <List.Content floated="left">
            <Header style={profileHeader} as="h4">
              Erfahrungspunkte bis zum nächsten Level:{" "}
              {userData.experienceTillNextLevel}
            </Header>
          </List.Content>
        </List.Item>
        <List.Item>
          <List.Content floated="left">
            <Header style={profileHeader} as="h4">
              Aktive Quests: {userData.activeQuests}
            </Header>
          </List.Content>
        </List.Item>
        <List.Item>
          <List.Content floated="left">
            <Header style={profileHeader} as="h4">
              Abgeschlossene Quests: {userData.finishedQuests}
            </Header>
          </List.Content>
        </List.Item>
        <List.Item />
        <List.Item />
        <List.Item />
        <List.Item />
        <List.Item />
        <br />
        <Divider style={divider} />

        <List.Item>
          <List.Content floated="right">
            <Button disabled={!changed} onClick={saveBtn}>
              Speichern
            </Button>
            <Button disabled={!changed} onClick={resetBtn}>
              Änderungen verwerfen
            </Button>
          </List.Content>
        </List.Item>
      </List>
    </Container>
  );
};

export default ProfileComponent;
