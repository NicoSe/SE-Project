//===============================================\\
//            ProfileComponent.tsx               \\
//===============================================\\

import { textShadow } from "../../global-styles";

export const profileContainer = {
  textShadow: textShadow,
};

export const profileHeader = {
  textShadow: textShadow,
};

export const profileHeaderHeading = {
  textShadow: textShadow,
};

export const profileTable = {
  border: "none",
};
