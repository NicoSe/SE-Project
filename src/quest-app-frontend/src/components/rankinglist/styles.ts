//===============================================\\
//               Rankinglist.tsx                 \\
//===============================================\\

import { textShadow } from "../../global-styles";

export const rankinglistContainer = {};

export const rankinglistHeader = {
  textShadow: textShadow,
};

export const rankinglistList = {
  maxHeight: "45vh",
  overflow: "auto",
  textShadow: textShadow,
};

export const rankinglistItem = {
  textShadow: textShadow,
};

export const rankinglistRank = {
  textShadow: textShadow,
};
