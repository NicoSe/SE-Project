import React from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { Container, Divider, Header, List } from "semantic-ui-react";
import { UserApi } from "../../gen/api";
import {
  popupActiveState,
  popupContentState,
  rankingState,
} from "../../states";
import { GetAPIConfig } from "../helpers";
import { divider } from "../../pages/styles";
import { handleError } from "../utils/ErrorHandler";
import {
  rankinglistContainer,
  rankinglistHeader,
  rankinglistItem,
  rankinglistList,
  rankinglistRank,
} from "./styles";

const Rankinglist: React.FC = () => {
  const ranks = useRecoilValue(rankingState);
  const setPopupActive = useSetRecoilState(popupActiveState);
  const setPopupContent = useSetRecoilState(popupContentState);

  // open popup with shortUser content
  const showUserDetails = async (playerName: string) => {
    const userApi = new UserApi(GetAPIConfig());
    try {
      const userDetail = await userApi.userByNamePlayerNameGet(playerName);
      setPopupContent(userDetail);
      setPopupActive(true);
    } catch (e) {
      handleError(e);
    }
  };

  return (
    <Container style={rankinglistContainer}>
      <Header as="h2" textAlign="center" style={rankinglistHeader}>
        Rangliste (Level)
      </Header>
      <Divider style={divider} />
      <List ordered style={rankinglistList}>
        {ranks.users.length === 0 ? (
          <List.Item style={rankinglistItem}>
            <List.Content floated="left" as="h4">
              Keine Daten vorhanden
            </List.Content>
          </List.Item>
        ) : (
          ranks.users.map((rank, id) => (
            <List.Item
              key={id}
              style={rankinglistItem}
              onClick={() => showUserDetails(rank.playerName)}
            >
              <List.Content className="popupLink" floated="left" as="h4">
                {rank.playerName + " (" + rank.level + ")"}
              </List.Content>
            </List.Item>
          ))
        )}
      </List>
      <Divider style={divider} />
      <Header as="h3" textAlign="right" style={rankinglistRank}>
        Dein Rang: {ranks.ownRank}
      </Header>
    </Container>
  );
};

export default Rankinglist;
