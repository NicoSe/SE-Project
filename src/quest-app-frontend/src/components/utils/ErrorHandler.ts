import { navigate } from "@reach/router";
import { logout } from "../helpers";
import { ApiException } from "../../gen/api";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

// activate toast based on http response.
// function is not called when http response was successful
export const handleError = async (e: Error, identifier?: string) => {
  console.log(e);
  if (e.message === "Failed to fetch") {
    toast("Server ist zur Zeit nicht erreichbar.", {
      type: "error",
      position: "bottom-right",
    });
  }
  if (e instanceof ApiException) {
    if (e.code === 400) {
      if (identifier === "reward") {
        toast("Du hast heute bereits einmal deine Belohnung abgeholt!", {
          type: "info",
          position: "bottom-right",
        });
      } else if (identifier === "addQuest") {
        toast("Du hast die maximale Anzahl von anpinnbaren Quests erreicht!", {
          type: "error",
          position: "bottom-right",
        });
      } else if (identifier === "shop") {
        toast("Du hast nicht genügend Geld, um diesen Artikel zu kaufen!", {
          type: "error",
          position: "bottom-right",
        });
      } else {
        toast(e.body["errors"][0], {
          type: "error",
          position: "bottom-right",
        });
      }
    } else if (e.code === 401) {
      if (identifier === "auth")
        toast("Nutzer oder Password sind nicht korrekt!", {
          type: "error",
          position: "bottom-right",
        });
      else {
        logout();
        navigate("/login");
      }
    } else if (e.code === 404) {
      if (identifier === "archenemy") {
        toast(
          "Dieser Nutzer exsistiert nicht oder es wurde kein Erzfeind hinterlegt.",
          {
            type: "error",
            position: "bottom-right",
          }
        );
      } else if (identifier === "auth") {
        toast("Erzfeind exsistiert nicht.", {
          type: "error",
          position: "bottom-right",
        });
      } else {
        toast(e.body["errors"][0], {
          type: "error",
          position: "bottom-right",
        });
      }
    } else if (e.code === 500) {
      toast.dismiss();
      toast(
        "Ein Serverfehler ist aufgetreten, bitte melden Sie sich neu an oder versuchen es später erneut.",
        {
          type: "error",
          position: "bottom-right",
        }
      );
    }
  } else {
    logout();
    navigate("/login");
  }
};
