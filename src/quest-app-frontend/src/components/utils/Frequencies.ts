export const frequencies = [
  { key: "1", value: "DAILY", test: "Täglich" },
  { key: "2", value: "TWO_DAYS", test: "2 Tage" },
  { key: "3", value: "THREE_DAYS", test: "3 Tage" },
  { key: "4", value: "WEEKLY", test: "Wöchentlich" },
  { key: "5", value: "TWO_WEEKS", test: "2 Wochen" },
];
