import { Container, Divider, Header } from "semantic-ui-react";
import { Reward } from "../../gen/api";
import PopupCloseBtn from "./PopupCloseBtn";
import { popupInnerRewardContainer, popupContentContainer, popupHeader } from "./styles";

interface IRewardPopupProps {
  reward: Reward;
}

// component that can display the content of rewards
const RewardPopup: React.FC<IRewardPopupProps> = (props) => {
  return (
    <Container style={popupInnerRewardContainer}>
      <PopupCloseBtn />
      <Container style={popupContentContainer}>
        <Header style={popupHeader} as="h2">
          Belohnung
        </Header>
        <Divider horizontal>Gold</Divider>
        {props.reward.gold}
        <br />
        <Divider horizontal>Experience</Divider>
        {props.reward.experience}
        <br />
      </Container>
    </Container>
  );
};

export default RewardPopup;
