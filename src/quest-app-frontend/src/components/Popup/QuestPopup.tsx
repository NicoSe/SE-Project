import { useSetRecoilState } from "recoil";
import { Container, Divider, Header } from "semantic-ui-react";
import { Quest, QuestApi } from "../../gen/api";
import { questsState } from "../../states";
import AddQuestBtn from "../buttons/AddQuestBtn";
import { GetAPIConfig } from "../helpers";
import { handleError } from "../utils/ErrorHandler";
import PopupCloseBtn from "./PopupCloseBtn";
import { popupInnerContainer, popupContentContainer, popupHeader } from "./styles";

interface IQuestPopupProps {
  quest: Quest;
}

// component that can display the content of quests
const QuestPopup: React.FC<IQuestPopupProps> = (props) => {
  const setQuests = useSetRecoilState(questsState);

  async function fetchQuestList(searchText: string) {
    const questApi = new QuestApi(GetAPIConfig());

    try {
      const quests = await questApi.questsGet(searchText);
      setQuests(quests);
    } catch (e) {
      handleError(e, "quest");
    }
  }

  return (
    <Container style={popupInnerContainer}>
      <PopupCloseBtn />
      <Container style={popupContentContainer}>
        <Header style={popupHeader} as="h2">
          {props.quest.title}
        </Header>
        <br />
        <Divider horizontal>Beschreibung</Divider>
        {props.quest.details === "" ? "- - -" : props.quest.details}
        <br />
        <Divider horizontal>Gold pro Ablieferung</Divider>
        {props.quest.goldPerCycle}
        <br />
        <Divider horizontal>Erfahrungspunkte pro Ablieferung</Divider>
        {props.quest.experiencePerCycle}
        <br />
      </Container>
      <Container>
        <AddQuestBtn
          quest={props.quest}
          updateQuestList={() => fetchQuestList("")}
        />
      </Container>
    </Container>
  );
};

export default QuestPopup;
