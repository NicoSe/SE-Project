import questBg from "../../assets/quest.png";
import questFinishedBg from "../../assets/questFinished.png";

//===============================================\\
//                  Popup.tsx                    \\
//===============================================\\

export const popupContainer = {
  position: "absolute",
  top: "50%",
  left: "50%",
  width: "100%",
  height: "100%",
  msTransform: "translate(-50%, -50%)",
  transform: "translate(-50%, -50%)",
  backgroundColor: "rgba(0,0,0,0.6)",
  textAlign: "center",
  zIndex: "10",
};

export const popupInnerContainer = {
  marginTop: "15vh",
  height: "70vh",
  width: "30%",
  backgroundImage: `url(${questBg})`,
  backgroundSize: "contain",
  backgroundPosition: "center top",
  backgroundRepeat: "no-repeat",
};

export const popupInnerRewardContainer = {
  marginTop: "15vh",
  height: "70vh",
  width: "30%",
  backgroundImage: `url(${questFinishedBg})`,
  backgroundSize: "contain",
  backgroundPosition: "center top",
  backgroundRepeat: "no-repeat",
};

export const popupContentContainer = {
  display: "inline-block",
  marginTop: "2vh",
  width: "23vw",
  maxHeight: "50vh",
  height: "50vh",
  overflow: "auto",
};

export const popupHeader = {};

export const closeButton = {
  marginRight: "3vw",
  marginTop: "2.8vh",
};
