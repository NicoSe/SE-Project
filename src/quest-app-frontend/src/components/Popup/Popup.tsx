import React from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useRecoilState } from "recoil";
import { Container } from "semantic-ui-react";
import { Quest, Reward, ShortUser, Todo } from "../../gen/api";
import { popupActiveState, popupContentState } from "../../states";
import QuestPopup from "./QuestPopup";
import RewardPopup from "./RewardPopup";
import ShortUserPopup from "./ShortUserPopup";
import * as styles from "./styles";
import TodoPopup from "./TodoPopup";

const Popup: React.FC = () => {
  const [active, setActive] = useRecoilState(popupActiveState);
  const [content, setContent] = useRecoilState(popupContentState);

  if (!active) return null;

  // select content display component
  if (content instanceof ShortUser) {
    return (
      <Container style={styles.popupContainer}>
        <ShortUserPopup user={content} />
      </Container>
    );
  } else if (content instanceof Todo) {
    return (
      <Container style={styles.popupContainer}>
        <TodoPopup todo={content} />
      </Container>
    );
  } else if (content instanceof Quest) {
    return (
      <Container style={styles.popupContainer}>
        <QuestPopup quest={content} />
      </Container>
    );
  } else if (content instanceof Reward) {
    return (
      <Container style={styles.popupContainer}>
        <RewardPopup reward={content} />
      </Container>
    );
  } else {
    setContent({});
    setActive(false);
    toast.dismiss();
    toast(
      "Ein Serverfehler ist aufgetreten, bitte melden Sie sich neu an oder versuchen es später erneut.",
      {
        type: "error",
        position: "bottom-right",
      }
    );
    return null;
  }
};

export default Popup;
