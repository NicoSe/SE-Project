import { useRecoilValue } from "recoil";
import { Container, Divider, Header, Image } from "semantic-ui-react";
import { ShortUser } from "../../gen/api";
import { configState } from "../../states";
import ShowArchenemyBtn from "../buttons/ShowArchenemyBtn";
import { translateProfileImageId } from "../helpers";
import PopupCloseBtn from "./PopupCloseBtn";
import { popupInnerContainer, popupContentContainer, popupHeader } from "./styles";

interface IShortUserPopupProps {
  user: ShortUser;
}

// component that can display the content of ShortUser
const ShortUserPopup: React.FC<IShortUserPopupProps> = (props) => {
  return (
    <Container style={popupInnerContainer}>
      <PopupCloseBtn />
      <Container style={popupContentContainer}>
        <Header style={popupHeader} as="h2">
          {props.user.playerName}
        </Header>
        <br />
        <Divider horizontal>Level</Divider>
        {props.user.level}
        <br />
        <Divider horizontal>Aktive Todos</Divider>
        {props.user.activeQuests}
        <br />
        <Divider horizontal>Abgeschlossene Quests</Divider>
        {props.user.finishedQuests}
        <br />
        <Divider horizontal>Erzfeind</Divider>
        {props.user.archenemy !== undefined && props.user.archenemy !== ""
          ? props.user.archenemy
          : props.user.playerName + " hat noch keinern Erzfeind erwählt."}
        <br />
        <br />
        <ShowArchenemyBtn
          archenemy={
            props.user.archenemy !== undefined ? props.user.archenemy : ""
          }
        />
      </Container>
    </Container>
  );
};

export default ShortUserPopup;
