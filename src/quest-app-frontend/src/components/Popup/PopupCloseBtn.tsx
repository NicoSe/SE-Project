import { useSetRecoilState } from "recoil";
import { List, Button } from "semantic-ui-react";
import { popupActiveState, popupContentState } from "../../states";
import { closeButton } from "./styles";

const PopupCloseBtn: React.FC = () => {
  const setActive = useSetRecoilState(popupActiveState);
  const setContent = useSetRecoilState(popupContentState);

  const close = () => {
    setContent({});
    setActive(false);
  };

  return (
    <List>
      <List.Item>
        <List.Content floated="right">
          <Button icon="close" onClick={() => close()} style={closeButton} />
        </List.Content>
      </List.Item>
    </List>
  );
};

export default PopupCloseBtn;
