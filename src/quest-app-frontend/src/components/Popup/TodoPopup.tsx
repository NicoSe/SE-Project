import { useState } from "react";
import { useSetRecoilState } from "recoil";
import { Container, Header, Divider, Dropdown } from "semantic-ui-react";
import { FrequencyEnum, QuestApi, Todo, UserApi } from "../../gen/api";
import { userState, rankingState, todosState } from "../../states";
import CloseTodoBtn from "../buttons/CloseTodoBtn";
import DeleteTodoBtn from "../buttons/DeleteTodoBtn";
import StartTodoBtn from "../buttons/StartTodoBtn";
import UpdateTodoBtn from "../buttons/UpdateTodoBtn";
import { GetAPIConfig } from "../helpers";
import { handleError } from "../utils/ErrorHandler";
import { frequencies } from "../utils/Frequencies";
import PopupCloseBtn from "./PopupCloseBtn";
import {
    popupInnerContainer,
    popupContentContainer,
    popupHeader,
} from "./styles";


interface ITodoPopupProps {
    todo: Todo;
}

// component that can display the content of todos
const TodoPopup: React.FC<ITodoPopupProps> = (props) => {
    const setUser = useSetRecoilState(userState);
    const setRanking = useSetRecoilState(rankingState);
    const setTodos = useSetRecoilState(todosState);
    const [frequency, setFrequency] = useState<FrequencyEnum>("DAILY");

    const fetchUserAndRanking = async () => {
        const userApi = new UserApi(GetAPIConfig());
        try {
            const res = await userApi.userGet();
            setUser({
                authenticated: true,
                data: res,
            });
            const ranking = await userApi.userRanksGet();
            setRanking(ranking);
        } catch (e) {
            handleError(e);
        }
    };

    const fetchTodoList = async () => {
        const questApi = new QuestApi(GetAPIConfig());

        try {
            const todos = await questApi.questsTodosGet();
            setTodos(todos);
        } catch (e) {
            handleError(e);
        }
    };

    return (
        <Container style={popupInnerContainer}>
            <PopupCloseBtn />
            <Container style={popupContentContainer}>
                <Header style={popupHeader} as="h2">
                    {props.todo.title}
                </Header>
                <br />
                <Divider horizontal>Status</Divider>
                {props.todo.status}
                <br />
                <Divider horizontal>Beschreibung</Divider>
                {props.todo.details === "" ? "- - -" : props.todo.details}
                <br />
                {props.todo.status === "ACTIVE" ? (
                    <Container>
                        <Divider horizontal>Annahmedatum</Divider>
                        {new Date(
                            Date.parse(
                                props.todo.startDate != undefined ? props.todo.startDate : ""
                            )
                        ).toLocaleDateString()}
                        <br />
                    </Container>
                ) : null}
                <Divider horizontal>Wiederholung</Divider>
                {props.todo.status === "PLANNED" ? (
                    <Dropdown
                        placeholder="Währe eine Frequenz"
                        fluid
                        selection
                        value={frequency}
                        options={frequencies}
                        onChange={(e, data) => {
                            e.preventDefault();
                            if (data.value === "DAILY") setFrequency("DAILY");
                            else if (data.value === "TWO_DAYS") setFrequency("TWO_DAYS");
                            else if (data.value === "THREE_DAYS") setFrequency("THREE_DAYS");
                            else if (data.value === "WEEKLY") setFrequency("WEEKLY");
                            else if (data.value === "TWO_WEEKS") setFrequency("TWO_WEEKS");
                            else setFrequency("DAILY");
                        }}
                    />
                ) : (
                    props.todo.frequency
                )}
                <br />
                <Divider horizontal>Gold pro Ablieferung</Divider>
                {props.todo.goldPerCycle}
                <br />
                <Divider horizontal>Erfahrungspunkte pro Ablieferung</Divider>
                {props.todo.experiencePerCycle}
                <br />
                {props.todo.status === "ACTIVE" ? (
                    <Container>
                        <Divider horizontal>
                            Nächste Möglichkeit eine Entlohnung zu fordern
                        </Divider>
                        {new Date(
                            Date.parse(
                                props.todo.availableClaim != undefined
                                    ? props.todo.availableClaim
                                    : ""
                            )
                        ).toLocaleDateString()}
                        <br />
                    </Container>
                ) : null}
            </Container>
            {props.todo.status === "ACTIVE" ? (
                <Container>
                    <UpdateTodoBtn
                        todo={props.todo}
                        updateUser={() => fetchUserAndRanking()}
                        updateTodoList={() => fetchTodoList()}
                    />
                    <CloseTodoBtn
                        todo={props.todo}
                        updateTodoList={() => fetchTodoList()}
                    />
                    <DeleteTodoBtn
                        todo={props.todo}
                        updateTodoList={() => fetchTodoList()}
                    />
                </Container>
            ) : null}
            {props.todo.status === "PLANNED" ? (
                <Container>
                    <StartTodoBtn
                        todo={props.todo}
                        updateTodoList={() => fetchTodoList()}
                        frequency={frequency}
                    />
                    <DeleteTodoBtn
                        todo={props.todo}
                        updateTodoList={() => fetchTodoList}
                    />
                </Container>
            ) : null}
        </Container>
    );
};

export default TodoPopup;
