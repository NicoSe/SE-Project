import { navigate, Router } from "@reach/router";
import { useEffect } from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useRecoilState } from "recoil";
import { Container } from "semantic-ui-react";
import "./components/extraStyles.css";
import { ContentMain } from "./pages/ContentMain";
import LoginPage from "./pages/LoginPage";
import { Profile } from "./pages/Profile";
import { Quests } from "./pages/Quests";
import { Shop } from "./pages/Shop";
import { Todos } from "./pages/Todos";
import { configState, userState } from "./states";

import boardImage from "./assets/board1.png";
import { translateBackgroundImageId } from "./components/helpers";
import { Inventory } from "./pages/Inventory";
import Popup from "./components/Popup/Popup";

// component that holds the basic structure of the app and decide if Login is displayed or the application
const App = () => {
  const [user] = useRecoilState(userState);
  const [config] = useRecoilState(configState);

  // styles
  const appContainer = {
    height: "100vh",
    backgroundImage: `url(${translateBackgroundImageId(
      config.backgroundImageId
    )})`,
    backgroundSize: "cover",
    backgroundPosition: "center",
    position: "absolute",
  };

  const appContainer2 = {
    height: "100vh",
    backgroundImage: `url(${boardImage})`,
    backgroundSize: "90vw 100vh",
    backgroundRepeat: "no-repeat",
    position: "absolute",
    bottom: 0,
    left: "5vw",
  };

  const checkAuthenticated = async () => {
    if (user.authenticated) {
      navigate("/todos");
    } else {
      navigate("/login");
    }
  };

  useEffect(() => {
    checkAuthenticated();
  }, []);

  return (
    <Container fluid style={appContainer}>
      <Container fluid style={appContainer2}></Container>
      <ToastContainer position="bottom-right" limit={3} />
      <Router>
        <LoginPage path="/login" />
        <LoginPage path="/register" />
        {user.authenticated ? (
          <ContentMain path="/">
            <Todos default path="/todos" />
            <Quests path="/quests" />
            <Profile path="/profile" />
            <Shop path="/shop" />
            <Inventory path="/inventory" />
          </ContentMain>
        ) : null}
      </Router>
      <Popup />
    </Container>
  );
};

export default App;
