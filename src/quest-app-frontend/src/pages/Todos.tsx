import * as React from "react";
import { RouteComponentProps } from "@reach/router";
import TodosComponent from "../components/todos/TodosComponent";
import { Container } from "semantic-ui-react";

export const Todos: React.FC<RouteComponentProps> = () => {
  return (
    <Container>
      <TodosComponent></TodosComponent>
    </Container>
  );
};
