import { textShadow } from "../global-styles";

export const divider = {
  borderTop: "2px solid black",
  marginTop: "0px",
  textShadow: textShadow,
};

//===============================================\\
//                LoginPage.tsx                  \\
//===============================================\\
export const loginGridStyle = {
  height: "100vh",
};
export const loginSegment = {
  backgroundColor: "rgba(124, 176, 255, 0.5)",
  border: "solid black",
  borderWidth: "1px",
};

//===============================================\\
//                ContentMain.tsx                \\
//===============================================\\

export const contentContainer = {
  width: "100%",
  height: "100%",
};

export const contentFooter = {
  textAlign: "center",
  position: "absolute",
  bottom: 0,
};
