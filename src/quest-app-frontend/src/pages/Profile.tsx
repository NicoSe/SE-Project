import * as React from "react";
import { RouteComponentProps } from "@reach/router";
import ProfileComponent from "../components/profile/ProfileComponent";
import { Container } from "semantic-ui-react";

export const Profile: React.FC<RouteComponentProps> = () => {
  return (
    <Container>
      <ProfileComponent></ProfileComponent>
    </Container>
  );
};
