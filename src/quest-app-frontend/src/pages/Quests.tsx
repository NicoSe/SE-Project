import * as React from "react";
import { RouteComponentProps } from "@reach/router";
import QuestsComponent from "../components/quests/QuestsComponent";
import { Container } from "semantic-ui-react";

export const Quests: React.FC<RouteComponentProps> = () => {
  return (
    <Container>
      <QuestsComponent></QuestsComponent>
    </Container>
  );
};
