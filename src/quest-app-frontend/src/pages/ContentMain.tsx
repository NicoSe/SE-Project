import { RouteComponentProps } from "@reach/router";
import React, { useEffect } from "react";
import { useSetRecoilState } from "recoil";
import { Container, Grid } from "semantic-ui-react";
import { MainHeader } from "../components/header/Header";
import { GetAPIConfig } from "../components/helpers";
import MainNavigation from "../components/navigation/Navigation";
import Rankinglist from "../components/rankinglist/Rankinglist";
import { handleError } from "../components/utils/ErrorHandler";
import { RankUser, UserApi } from "../gen/api";
import { configState, rankingState, userState } from "../states";
import { contentContainer, contentFooter } from "./styles";

// component that manages basic structure of the application
export const ContentMain: React.FC<RouteComponentProps> = (props) => {
  const setRankings = useSetRecoilState(rankingState);
  const setUser = useSetRecoilState(userState);
  const setConfig = useSetRecoilState(configState);

  // initial fetch of user
  async function fetchUser() {
    const userApi = new UserApi(GetAPIConfig());

    try {
      const user = await userApi.userGet();
      setUser({
        authenticated: true,
        data: user,
      });
    } catch (e) {
      handleError(e);
    }
  }

  // initial fetch of ranking
  async function fetchRankingList() {
    const userApi = new UserApi(GetAPIConfig());

    try {
      const ranks = await userApi.userRanksGet();
      if (ranks.users.length === 0) {
        const msg = new RankUser();
        msg.playerName = "Keine Daten verfügbar";
        ranks.users.concat(msg);
      }
      setRankings(ranks);
    } catch (e) {
      handleError(e);
    }
  }

  // initial fetch of user config
  async function fetchUserConfig() {
    const userApi = new UserApi(GetAPIConfig());

    try {
      const userConfig = await userApi.userConfigGet();
      setConfig(userConfig);
    } catch (e) {
      handleError(e);
    }
  }

  useEffect(() => {
    fetchUser();
    fetchRankingList();
    fetchUserConfig();
  }, []);

  return (
    <Container fluid>
      <MainHeader />
      <Container fluid style={contentContainer}>
        <Grid centered>
          <Grid.Row columns={3}>
            <Grid.Column width={2} textAlign="right">
              <MainNavigation />
            </Grid.Column>
            <Grid.Column width={8} textAlign="center">
              {props.children}
            </Grid.Column>
            <Grid.Column width={2} textAlign="right">
              <Rankinglist />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
      <Container fluid as="h3" style={contentFooter}>
        Nico Seng - 76628 - SE-Project
      </Container>
    </Container>
  );
};
