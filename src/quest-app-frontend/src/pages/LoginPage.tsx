import { navigate, RouteComponentProps } from "@reach/router";
import React, { MouseEvent, useState } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useRecoilState } from "recoil";
import {
  Button,
  Container,
  Form,
  Grid,
  Header,
  Input,
  Segment,
} from "semantic-ui-react";
import { ACCESS_TOKEN_KEY, GetAPIConfig, login } from "../components/helpers";
import { handleError } from "../components/utils/ErrorHandler";
import { UserApi } from "../gen/api";
import { textShadow } from "../global-styles";
import { userState } from "../states";
import { loginGridStyle, loginSegment } from "./styles";

const LoginPage: React.FC<RouteComponentProps> = (props) => {
  //manage state
  const [user, setUser] = useRecoilState(userState);
  const [playerName, setPlayerName] = useState<string>("");
  const [archenemy, setArchenemy] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [passwordAgain, setPasswordAgain] = useState<string>("");
  const [submitted, setSubmitted] = useState<boolean>(false);

  // if authenticated jump forward
  if (user.authenticated && localStorage.getItem(ACCESS_TOKEN_KEY) !== null) {
    navigate("/todos");
    return <></>;
  }

  // button actions
  const changeToLogin = async (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    navigate("/login");
  };

  const changeToRegister = async (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    navigate("/register");
  };

  const submitRegister = async (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    // prevent user from double click
    if (submitted) {
      return;
    }
    setSubmitted(true);

    if (password !== passwordAgain) {
      toast("Passwörter stimmen nicht überein!", {
        type: "error",
        position: "bottom-right",
      });
      setSubmitted(true);
      return;
    }

    try {
      // create new user request
      var userApi = new UserApi(GetAPIConfig());
      await userApi.userPost({
        playerName: playerName,
        email: email,
        archenemy: archenemy,
        password: password,
      });
    } catch (e) {
      handleError(e, "auth");
      setSubmitted(false);
      return;
    }

    try {
      // login user to get JWT
      await login(email, password);

      setUser({
        authenticated: true,
      });

      navigate("/todos");
    } catch (e) {
      setSubmitted(false);
    }
  };

  const submitLogin = async (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    // prevent user from double click
    if (submitted) {
      return;
    }
    setSubmitted(true);

    try {
      await login(email, password);

      setUser({
        authenticated: true,
      });

      navigate("/todos");
    } catch (e) {
      setSubmitted(false);
    }
  };

  if (props.location?.pathname === "/register") {
    return (
      <Container fluid>
        <Grid textAlign="center" style={loginGridStyle} verticalAlign="middle">
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h2" style={{ textShadow: textShadow }}>
              Gib deine Benutzerdaten an, um einen Benutzer zu erstellen.
            </Header>
            <Form>
              <Segment style={loginSegment}>
                <Input
                  fluid
                  focus
                  iconPosition="left"
                  icon="user"
                  placeholder="Spielername"
                  value={playerName}
                  loading={submitted}
                  disabled={submitted}
                  onChange={(e, data) => {
                    e.preventDefault();
                    setPlayerName(data.value);
                  }}
                />
                <br />

                <Input
                  fluid
                  iconPosition="left"
                  icon="at"
                  placeholder="E-Mail"
                  value={email}
                  loading={submitted}
                  disabled={submitted}
                  onChange={(e, data) => {
                    e.preventDefault();
                    setEmail(data.value);
                  }}
                />
                <br />

                <Input
                  fluid
                  iconPosition="left"
                  icon="address card"
                  placeholder="Erzfeind-Spielername (optional)"
                  value={archenemy}
                  loading={submitted}
                  disabled={submitted}
                  onChange={(e, data) => {
                    e.preventDefault();
                    setArchenemy(data.value);
                  }}
                />
                <br />

                <Input
                  type="password"
                  fluid
                  iconPosition="left"
                  icon="key"
                  placeholder="Password"
                  value={password}
                  loading={submitted}
                  disabled={submitted}
                  onChange={(e, data) => {
                    e.preventDefault();
                    setPassword(data.value);
                  }}
                />
                <br />

                <Input
                  type="password"
                  fluid
                  iconPosition="left"
                  icon="key"
                  placeholder="Password wiederholen"
                  value={passwordAgain}
                  loading={submitted}
                  disabled={submitted}
                  onChange={(e, data) => {
                    e.preventDefault();
                    setPasswordAgain(data.value);
                  }}
                />
                <br />
                <Button.Group fluid>
                  <Button positive onClick={submitRegister}>
                    {" "}
                    Registrieren{" "}
                  </Button>
                  <Button onClick={changeToLogin}>
                    {" "}
                    Ich habe bereits einen Account!{" "}
                  </Button>
                </Button.Group>
              </Segment>
            </Form>
          </Grid.Column>
        </Grid>
      </Container>
    );
  } else {
    return (
      <Container fluid>
        <Grid textAlign="center" style={loginGridStyle} verticalAlign="middle">
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h2" style={{ textShadow: textShadow }}>
              Gib deine Email und dein Password an, um dich anzumelden.
            </Header>
            <Form>
              <Segment style={loginSegment}>
                <Input
                  fluid
                  focus
                  iconPosition="left"
                  icon="at"
                  placeholder="E-Mail"
                  value={email}
                  loading={submitted}
                  disabled={submitted}
                  onChange={(e, data) => {
                    e.preventDefault();
                    setEmail(data.value);
                  }}
                />
                <br />

                <Input
                  type="password"
                  fluid
                  iconPosition="left"
                  icon="key"
                  placeholder="Password"
                  value={password}
                  loading={submitted}
                  disabled={submitted}
                  onChange={(e, data) => {
                    e.preventDefault();
                    setPassword(data.value);
                  }}
                />
                <br />
                <Button.Group fluid>
                  <Button positive onClick={submitLogin}>
                    {" "}
                    Anmelden{" "}
                  </Button>
                  <Button onClick={changeToRegister}>
                    {" "}
                    Ich habe noch keinen Account!{" "}
                  </Button>
                </Button.Group>
              </Segment>
            </Form>
          </Grid.Column>
        </Grid>
      </Container>
    );
  }
};

export default LoginPage;
