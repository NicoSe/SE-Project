import * as React from "react";
import { RouteComponentProps } from "@reach/router";
import { Container } from "semantic-ui-react";
import InventoryComponent from "../components/inventory/InventoryComponent";

export const Inventory: React.FC<RouteComponentProps> = () => {
  return (
    <Container>
      <InventoryComponent></InventoryComponent>
    </Container>
  );
};
