import * as React from "react";
import { RouteComponentProps } from "@reach/router";
import { Container } from "semantic-ui-react";
import ShopComponent from "../components/shop/ShopComponent";

export const Shop: React.FC<RouteComponentProps> = () => {
  return (
    <Container>
      <ShopComponent></ShopComponent>
    </Container>
  );
};
