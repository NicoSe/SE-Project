package main

import (
	"flag"
	"net/http"

	log "github.com/sirupsen/logrus"

	"gitlab.com/se-project/config"
	"gitlab.com/se-project/database"
	service "gitlab.com/se-project/microservices/userService"
	"gitlab.com/se-project/models"
	"gitlab.com/se-project/utils"
)

func main() {
	port := flag.String("port", "8080", "the used port")
	flag.Parse()

	cfg := config.New()

	db := database.New(cfg.DatabaseURI)
	database.Migrate(db, &models.User{})
	database.Migrate(db, &models.UserConfig{})

	// insert fake data
	db.Exec("INSERT INTO QuestApp.users (id, created_at, updated_at, deleted_at, player_name, email, password, gold, experience, `level`, archenemy) VALUES(1, NULL, NULL, NULL, 'Darth Vader', NULL, NULL, 2623, 2895, 56, 'IronMän');")
	db.Exec("INSERT INTO QuestApp.users (id, created_at, updated_at, deleted_at, player_name, email, password, gold, experience, `level`, archenemy) VALUES(2, NULL, NULL, NULL, 'T-Rex', NULL, NULL, 23, 20, 2, 'Ryshadium');")
	db.Exec("INSERT INTO QuestApp.users (id, created_at, updated_at, deleted_at, player_name, email, password, gold, experience, `level`, archenemy) VALUES(3, NULL, NULL, NULL, 'Legolas', NULL, NULL, 534, 700, 14, 'Silberblick');")
	db.Exec("INSERT INTO QuestApp.users (id, created_at, updated_at, deleted_at, player_name, email, password, gold, experience, `level`, archenemy) VALUES(4, NULL, NULL, NULL, 'user666', NULL, NULL, 223, 446, 8, 'Amethyst');")
	db.Exec("INSERT INTO QuestApp.users (id, created_at, updated_at, deleted_at, player_name, email, password, gold, experience, `level`, archenemy) VALUES(5, NULL, NULL, NULL, 'Ninja90', NULL, NULL, 123, 156, 3, NULL);")
	db.Exec("INSERT INTO QuestApp.users (id, created_at, updated_at, deleted_at, player_name, email, password, gold, experience, `level`, archenemy) VALUES(6, NULL, NULL, NULL, 'Amethyst', NULL, NULL, 573, 422, 8, NULL);")
	db.Exec("INSERT INTO QuestApp.users (id, created_at, updated_at, deleted_at, player_name, email, password, gold, experience, `level`, archenemy) VALUES(7, NULL, NULL, NULL, 'Ryshadium', NULL, NULL, 964, 1145, 22, NULL);")
	db.Exec("INSERT INTO QuestApp.users (id, created_at, updated_at, deleted_at, player_name, email, password, gold, experience, `level`, archenemy) VALUES(8, NULL, NULL, NULL, 'IronMän', NULL, NULL, 1734, 261, 5, 'Darth Vader');")
	db.Exec("INSERT INTO QuestApp.users (id, created_at, updated_at, deleted_at, player_name, email, password, gold, experience, `level`, archenemy) VALUES(9, NULL, NULL, NULL, 'Thorrr', NULL, NULL, 74, 79, 1, 'T-Rex');")
	db.Exec("INSERT INTO QuestApp.users (id, created_at, updated_at, deleted_at, player_name, email, password, gold, experience, `level`, archenemy) VALUES(10, NULL, NULL, NULL, 'Silberblick', NULL, NULL, 930, 1575, 30, 'T-Rex');")

	app := service.NewApp(cfg, db)
	router := service.GetRouter(app)
	log.Infof("Starting service on port: %s", *port)
	_ = http.ListenAndServe(":"+*port, utils.InitCORS()(router))
}
