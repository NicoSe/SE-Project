package main

import (
	"flag"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/se-project/utils"

	"gitlab.com/se-project/config"
	"gitlab.com/se-project/database"
	service "gitlab.com/se-project/microservices/questService"
	"gitlab.com/se-project/models"
)

func main() {
	port := flag.String("port", "8080", "the used port")
	flag.Parse()

	cfg := config.New()

	db := database.New(cfg.DatabaseURI)
	database.Migrate(db, &models.Todo{})
	database.Migrate(db, &models.Quest{})

	// insert fake data
	db.Exec("INSERT INTO QuestApp.quests (id, created_at, updated_at, deleted_at, title, details, gold_per_cycle, experience_per_cycle) VALUES(1, NULL, NULL, NULL, 'Kochen lernen', 'Einmal am Tag eine warme Malzeit zu sich zu nehmen ist sehr empfehlenswert. Und es ist um so besser, wenn das Essen nicht aus der Gefriertruhe kommt. Versuche doch jeden Tag etwas zu kochen.', 20, 17);")
	db.Exec("INSERT INTO QuestApp.quests (id, created_at, updated_at, deleted_at, title, details, gold_per_cycle, experience_per_cycle) VALUES(2, NULL, NULL, NULL, 'Geschichte vorlesen', 'Deinem Kind etwas vorzulesen fördert die Entwicklung des Kindes und übt dich selbst im fließenden Lesen.', 10, 12);")
	db.Exec("INSERT INTO QuestApp.quests (id, created_at, updated_at, deleted_at, title, details, gold_per_cycle, experience_per_cycle) VALUES(3, NULL, NULL, NULL, 'Pflanzen gießen', 'Erinnere dich daran wann du deine Pflanzen gießen solltest indem du es zu deinem Alltag machst. Denn Pflanzen haben auch ein Recht auf das Leben.', 5, 5);")
	db.Exec("INSERT INTO QuestApp.quests (id, created_at, updated_at, deleted_at, title, details, gold_per_cycle, experience_per_cycle) VALUES(4, NULL, NULL, NULL, 'Äpfel ernten', 'In dieser Aufgabe geht es darum so Äpfel zu sammeln und diese abzuliefern. Pro Woche, was ich empfehlen würde, sollten dabei 50 Äpfel zusammen kommen.', 20, 25);")
	db.Exec("INSERT INTO QuestApp.quests (id, created_at, updated_at, deleted_at, title, details, gold_per_cycle, experience_per_cycle) VALUES(5, NULL, NULL, NULL, 'Mama helfen', 'Sei freundlich un greif deiner Mama unter die Arme.', 0, 10);")
	db.Exec("INSERT INTO QuestApp.quests (id, created_at, updated_at, deleted_at, title, details, gold_per_cycle, experience_per_cycle) VALUES(6, NULL, NULL, NULL, 'Papa helfen', 'Sei freundlich un greif deinem Papa unter die Arme.', 10, 10);")
	db.Exec("INSERT INTO QuestApp.quests (id, created_at, updated_at, deleted_at, title, details, gold_per_cycle, experience_per_cycle) VALUES(7, NULL, NULL, NULL, 'Oma helfen', 'Sei freundlich un greif deiner Oma unter die Arme.', 10, 10);")
	db.Exec("INSERT INTO QuestApp.quests (id, created_at, updated_at, deleted_at, title, details, gold_per_cycle, experience_per_cycle) VALUES(8, NULL, NULL, NULL, 'Opa helfen', 'Sei freundlich un greif deinem Opa unter die Arme.', 10, 10);")
	db.Exec("INSERT INTO QuestApp.quests (id, created_at, updated_at, deleted_at, title, details, gold_per_cycle, experience_per_cycle) VALUES(9, NULL, NULL, NULL, 'Zimmer aufräumen', 'In einem sauberen Zimmer wohnt es sich besser. Man fühlt sich viel eher wie zuhause. Selbst wenn du keine Lust auf einen Großputz hast, kannst du versuchen dir anzutrainieren immer mal wieder etwas wegzuräumen. Du wirst sehen dabei staut sich viel weniger an.', 5, 7);")
	db.Exec("INSERT INTO QuestApp.quests (id, created_at, updated_at, deleted_at, title, details, gold_per_cycle, experience_per_cycle) VALUES(10, NULL, NULL, NULL, 'Nichts Süßes essen', 'Achte auf deine Gesundheit und iss weniger Süßes. ', 5, 8);")
	db.Exec("INSERT INTO QuestApp.quests (id, created_at, updated_at, deleted_at, title, details, gold_per_cycle, experience_per_cycle) VALUES(11, NULL, NULL, NULL, 'Küche aufräumen', 'In einer sauberen Küche kocht man viel eher und läd auch öfter Freunde ein. Versuche dir diese Gedanken im Gedächtnis zu halten und stehts nach dem Essen alles wegzuräumen.', 5, 7);")
	db.Exec("INSERT INTO QuestApp.quests (id, created_at, updated_at, deleted_at, title, details, gold_per_cycle, experience_per_cycle) VALUES(12, NULL, NULL, NULL, 'Putzen', 'Putzen macht am meisten Spaß, wenn man dabei Spaß hat und die Lust dazu verspürt. Versuche öfter den Schmutz in deiner Umgebung zu bemerken und gestalte dir deine Umgebung während des Putzens mit Musik oder ähnlichem.', 13, 10);")
	db.Exec("INSERT INTO QuestApp.quests (id, created_at, updated_at, deleted_at, title, details, gold_per_cycle, experience_per_cycle) VALUES(13, NULL, NULL, NULL, 'Ein Kapitel lesen', 'Regelmäßiges lesen sorgt nicht nur dafür, dass du tendenziell weniger an elektrischen Geräten sitzt sondern fördert auch deinen Intellekt. Versuche Bücher zu finden, die dir gefallen, um den Einstieg zu erleichtern.', 22, 30);")

	app := service.NewApp(cfg, db)
	router := service.GetRouter(app)
	log.Infof("Starting service on port: %s", *port)
	_ = http.ListenAndServe(":"+*port, utils.InitCORS()(router))
}
