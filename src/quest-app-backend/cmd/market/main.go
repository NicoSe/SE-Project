package main

import (
	"flag"
	"net/http"

	log "github.com/sirupsen/logrus"

	"gitlab.com/se-project/config"
	"gitlab.com/se-project/database"
	service "gitlab.com/se-project/microservices/marketService"
	"gitlab.com/se-project/models"
	"gitlab.com/se-project/utils"
)

func main() {
	port := flag.String("port", "8080", "the used port")
	flag.Parse()

	cfg := config.New()

	db := database.New(cfg.DatabaseURI)
	database.Migrate(db, &models.MarketInventar{})

	app := service.NewApp(cfg, db)
	router := service.GetRouter(app)
	log.Infof("Starting service on port: %s", *port)
	_ = http.ListenAndServe(":"+*port, utils.InitCORS()(router))
}
