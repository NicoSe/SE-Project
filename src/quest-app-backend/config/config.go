package config

import (
	"log"

	"github.com/spf13/viper"
)

type Config struct {
	DatabaseURI        string
	JWTSecret          string
	LevelFunction      float64
	MultiplicationRate float32
	MaxTodos           int
}

func New() *Config {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("fatal error config file: %s", err)
	}

	var cfg Config
	err = viper.Unmarshal(&cfg)
	if err != nil {
		log.Fatal("Fatal reading into config")
	}

	return &cfg
}
