## Available Scripts

In the project directory, you can run:

### `gorun./cmd/{service}/main.go-port8080`

Runs single services in the development mode.Decide which service you want to start with the variable {service}.\
Be aware, that you must change the port for every service, so that they not block each other.

You will also see any errors in the console.

### `docker-compose up -d`

Builds and creates docker images for this application.\
Server is available via [http://localhost:8000](http://localhost:8000). User Postman or Insomnia make requests.

Run 'docker-compose build --no-cache' if the images won´t build automatically.