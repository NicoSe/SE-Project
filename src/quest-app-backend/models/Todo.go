package models

import "time"

type Todo struct {
	Model

	Title              string        `json:"title"`
	Details            string        `json:"details"`
	Status             StatusEnum    `json:"status"`
	Frequency          FrequencyEnum `json:"frequency"`
	StartDate          time.Time     `json:"startDate"`
	AvailableClaim     time.Time     `json:"availableClaim"`
	GoldPerCycle       int           `json:"goldPerCycle"`
	ExperiencePerCycle int           `json:"experiencePerCycle"`
	Multiplication     float32       `json:"multiplication"`
	UserId             uint          `json:"-"`
}

type TodoListItem struct {
	Model

	Title          string     `json:"title"`
	AvailableClaim time.Time  `json:"availableClaim"`
	Status         StatusEnum `json:"status"`
}
