package models

import (
	"time"

	"gorm.io/gorm"
)

type UserConfig struct {
	CreatedAt time.Time      `json:"-"`
	UpdatedAt time.Time      `json:"-"`
	DeletedAt gorm.DeletedAt `json:"-" gorm:"index"`

	UserId            uint   `json:"-" gorm:"primarykey"`
	BackgroundImageId string `json:"backgroundImageId"`
	ProfilImageId     string `json:"profileImageId"`
}
