package models

type StatusEnum string

const (
	ACTIVE   StatusEnum = "ACTIVE"
	FINISHED StatusEnum = "FINISHED"
	PLANNED  StatusEnum = "PLANNED"
)

type FrequencyEnum string

const (
	DAILY  FrequencyEnum = "DAILY"
	TWO_DAYS  FrequencyEnum = "TWO_DAYS"
	THREE_DAYS  FrequencyEnum = "THREE_DAYS"
	WEEKLY FrequencyEnum = "WEEKLY"
	TWO_WEEKS FrequencyEnum = "TWO_WEEKS"
)
