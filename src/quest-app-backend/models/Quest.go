package models

type Quest struct {
	Model

	Title              string `json:"title"`
	Details            string `json:"details"`
	GoldPerCycle       int    `json:"goldPerCycle"`
	ExperiencePerCycle int    `json:"experiencePerCycle"`
}

type QuestListItem struct {
	Model

	Title string `json:"title"`
}

func (quest Quest) TransformToTodo(userId uint) Todo {
	return Todo{UserId: userId, Title: quest.Title, Details: quest.Details, Status: "PLANNED", Multiplication: 1, ExperiencePerCycle: quest.ExperiencePerCycle,
		GoldPerCycle: quest.GoldPerCycle, Frequency: "DAILY"}
}
