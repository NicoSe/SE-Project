package models

type Reward struct {
	AmountOfMoney      int `json:"gold"`
	AmountOfExperience int `json:"experience"`
}
