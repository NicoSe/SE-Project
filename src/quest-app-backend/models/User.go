package models

type User struct {
	Model

	PlayerName string `json:"playerName"`
	Email      string `json:"email"`
	Password   string `json:"-"`
	Gold       int    `json:"gold"`
	Experience int    `json:"experience"`
	Level      int    `json:"level"`
	Archenemy  string `json:"archenemy"`
}

type UserDetails struct {
	ShortUser

	Experience              int `json:"experience"`
	ExperienceTillNextLevel int `json:"experienceTillNextLevel"`
	Gold                    int `json:"gold"`
}

type RankUser struct {
	PlayerName string `json:"playerName"`
	Level      int    `json:"level"`
}

type Ranking struct {
	Users   []RankUser `json:"users"`
	OwnRank string     `json:"ownRank"`
}

type ShortUser struct {
	PlayerName     string `json:"playerName"`
	Email          string `json:"email"`
	Level          int    `json:"level"`
	ActiveQuests   int    `json:"activeQuests"`
	FinishedQuests int    `json:"finishedQuests"`
	Archenemy      string `json:"archenemy"`
}

type CreateUpdateUser struct {
	PlayerName string `json:"playerName"`
	Email      string `json:"email"`
	Password   string `json:"password"`
	Archenemy  string `json:"archenemy"`
}

func (user User) TransformToShortUser() ShortUser {
	return ShortUser{PlayerName: user.PlayerName, Email: user.Email, Archenemy: user.Archenemy, Level: user.Level}
}

func (user User) TransformToUserDetails() UserDetails {
	return UserDetails{
		ShortUser:  user.TransformToShortUser(),
		Gold:       user.Gold,
		Experience: user.Experience,
	}
}

func (user CreateUpdateUser) TransformToUser() User {
	return User{PlayerName: user.PlayerName, Email: user.Email, Password: user.Password, Archenemy: user.Archenemy}
}
