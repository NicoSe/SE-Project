package models

type Error struct {
	Code   int      `json:"code"`
	Type   string   `json:"type"`
	Errors []string `json:"errors"`
}
