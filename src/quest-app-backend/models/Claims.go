package models

import "github.com/form3tech-oss/jwt-go"

type Claims struct {
	jwt.StandardClaims

	Id       uint `json:"id"`
	Username string `json:"username"`
}