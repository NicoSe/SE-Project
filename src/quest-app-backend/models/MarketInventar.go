package models

import (
	"time"

	"gorm.io/gorm"
)

type MarketInventar struct {
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"-" gorm:"index"`

	UserId                  uint   `json:"-" gorm:"primarykey"`
	BackgroundImageInventar string `json:"backgroundImageInventar"`
	ProfileImageInventar    string `json:"profileImageInventar"`
}

type Inventar struct {
	BackgroundImageInventar []string `json:"backgroundImageInventar"`
	ProfileImageInventar    []string `json:"profileImageInventar"`
}
