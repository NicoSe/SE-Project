package questService

import (
	"math"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/se-project/models"
	"gitlab.com/se-project/utils"
)

// Handler function for Get /quests endpoint
// return a list of all quest, besite the ones, the user has already accepted
// returns Quest list
func (app *App) QuestListHandler(w http.ResponseWriter, r *http.Request) {
	// Get JWT claims
	claims, err := utils.GetJWTClaims(r)
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, "internal server error")
		return
	}

	nameFilter := r.URL.Query().Get("nameFilter")

	// find quests
	var quests []models.QuestListItem
	if nameFilter != "" {
		app.db.Model(models.Quest{}).Find(&quests, "title LIKE ? OR details LIKE ?", "%"+nameFilter+"%", "%"+nameFilter+"%")
	} else {
		app.db.Model(models.Quest{}).Find(&quests)
	}

	// filter quests to not display ones that the user currently has activated
	var todos []models.Todo
	app.db.Find(&todos, "user_id = ? AND start_date <= ? AND status <> 'FINISHED'", claims.Id, time.Now())

	resultQuestList := make([]models.QuestListItem, 0)
	for quest := range quests {
		var duplicate = false
		for todo := range todos {
			if quests[quest].Title == todos[todo].Title {
				duplicate = true
			}
		}
		if !duplicate {
			resultQuestList = append(resultQuestList, quests[quest])
		}
	}

	// create response
	utils.JSONResponse(w, http.StatusOK, resultQuestList)
}

// Handler function for Get /quests/{quest-id} endpoint
// return a specific quest
// returns Quest
func (app *App) QuestDetailHandler(w http.ResponseWriter, r *http.Request) {
	questId := mux.Vars(r)["quest-id"]

	var quest models.Quest
	app.db.Find(&quest, "id = ?", questId)
	if quest.ID == 0 {
		utils.JSONResponseError(w, http.StatusNotFound, "quest not found")
		return
	}

	// create response
	utils.JSONResponse(w, http.StatusOK, quest)
}

// Handler function for Get /quests/todos endpoint
// return a list of all todos a user has
// returns Todo list
func (app *App) TodoListHandler(w http.ResponseWriter, r *http.Request) {
	// Get JWT claims
	claims, err := utils.GetJWTClaims(r)
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, "internal server error")
		return
	}

	// find todos of user
	var todos []models.TodoListItem
	app.db.Model(models.Todo{}).Find(&todos, "user_id = ?", claims.Id)

	// create response
	if len(todos) == 0 {
		todos = make([]models.TodoListItem, 0)
	}
	utils.JSONResponse(w, http.StatusOK, todos)
}

// Handler function for GET /quests/todos/{todo-id} endpoint
// return a todo if the user has this one
// returns Todo
func (app *App) TodoDetailHandler(w http.ResponseWriter, r *http.Request) {
	// Get JWT claims
	claims, err := utils.GetJWTClaims(r)
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, "internal server error")
		return
	}

	todoId := mux.Vars(r)["todo-id"]

	var todo models.Todo
	app.db.Find(&todo, "id = ?", todoId)
	if todo.UserId != claims.Id {
		utils.JSONResponseError(w, http.StatusNotFound, "todo not found or no access right")
		return
	}

	// create response
	utils.JSONResponse(w, http.StatusOK, todo)
}

// Handler function for DELETE /quests/todos/{todo-id} endpoint
// delete a todo if it exists and is not closed yet
// returns
func (app *App) TodoAbortHandler(w http.ResponseWriter, r *http.Request) {
	// Get JWT claims
	claims, err := utils.GetJWTClaims(r)
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, "internal server error")
		return
	}

	todoId := mux.Vars(r)["todo-id"]

	var todo models.Todo
	app.db.Find(&todo, "id = ?", todoId)
	if todo.UserId != claims.Id {
		utils.JSONResponseError(w, http.StatusNotFound, "todo not found or no access right")
		return
	}

	if todo.Status == "FINISHED" {
		utils.JSONResponseError(w, http.StatusBadRequest, "finished todos can not be deleted")
		return
	}

	app.db.Delete(&todo)

	// create response
	w.WriteHeader(http.StatusNoContent)
}

// Handler function for POST /quests/todos/add/{quest-id} endpoint
// add a selected quest to your todos, but reject if user has too many todos or this todo is doubled
// returns
func (app *App) TodoAddQuestHandler(w http.ResponseWriter, r *http.Request) {
	// Get JWT claims
	claims, err := utils.GetJWTClaims(r)
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, "internal server error")
		return
	}

	questId := mux.Vars(r)["quest-id"]
	var quest models.Quest
	app.db.Find(&quest, "id = ?", questId)
	if quest.ID == 0 {
		utils.JSONResponseError(w, http.StatusNotFound, "quest not found")
		return
	}

	// check if quest is already in todos or there are to much todos
	var todos []models.Todo
	app.db.Find(&todos, "user_id = ? AND start_date <= ? AND status <> 'FINISHED'", claims.Id, time.Now())

	if len(todos) >= app.cfg.MaxTodos {
		utils.JSONResponseError(w, http.StatusBadRequest, "user can not accept more todos")
		return
	}
	for todo := range todos {
		if todos[todo].Title == quest.Title {
			utils.JSONResponseError(w, http.StatusBadRequest, "quest is already in your todos")
			return
		}
	}

	// create response
	newTodo := quest.TransformToTodo(claims.Id)
	if newTodo.Frequency == "TWO_DAYS" {
		newTodo.GoldPerCycle *= 2
		newTodo.ExperiencePerCycle *= 2
	} else if newTodo.Frequency == "THREE_DAYS" {
		newTodo.GoldPerCycle *= 3
		newTodo.ExperiencePerCycle *= 3
	} else if newTodo.Frequency == "WEEKLY" {
		newTodo.GoldPerCycle *= 7
		newTodo.ExperiencePerCycle *= 7
	} else if newTodo.Frequency == "TWO_WEEKS" {
		newTodo.GoldPerCycle *= 14
		newTodo.ExperiencePerCycle *= 14
	}
	app.db.Save(&newTodo)
	w.WriteHeader(http.StatusCreated)
}

// Handler function for PUT /quests/todo/{todo-id}/start endpoint
// start a quest based on a selected frequency and set it to active
// returns
func (app *App) TodoStartHandler(w http.ResponseWriter, r *http.Request) {
	// Get JWT claims
	claims, err := utils.GetJWTClaims(r)
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, "internal server error")
		return
	}

	todoId := mux.Vars(r)["todo-id"]
	frequency := r.URL.Query().Get("frequency")

	// check user and todos
	var todo models.Todo
	app.db.Find(&todo, "id = ?", todoId)
	if todo.UserId != claims.Id {
		utils.JSONResponseError(w, http.StatusNotFound, "todo not found or no access right")
		return
	}

	if todo.Status != "PLANNED" {
		utils.JSONResponseError(w, http.StatusBadRequest, "todo can not be started because it is either currently active or completed")
		return
	}

	todo.StartDate = time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC)
	todo.AvailableClaim = time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day()-1, 23, 59, 59, 0, time.UTC)
	todo.Status = "ACTIVE"
	todo.Frequency = models.FrequencyEnum(frequency)
	app.db.Save(&todo)

	// create response
	w.WriteHeader(http.StatusNoContent)
}

// Handler function for PUT /quests/todo/{todo-id}/finish endpoint
// check if a quest is active and can be finished based on the frequency and the last time it was finished, if so,
// refresh last finished date and calculate a reward for the player
// returns Reward
func (app *App) TodoUpdateHandler(w http.ResponseWriter, r *http.Request) {
	// Get JWT claims
	claims, err := utils.GetJWTClaims(r)
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, "internal server error")
		return
	}

	todoId := mux.Vars(r)["todo-id"]

	// check user and todos
	var todo models.Todo
	app.db.Find(&todo, "id = ?", todoId)
	if todo.UserId != claims.Id {
		utils.JSONResponseError(w, http.StatusNotFound, "todo not found or no access right")
		return
	}

	var user models.User
	app.db.Find(&user, "id = ?", claims.Id)
	if user.ID == 0 {
		utils.JSONResponseError(w, http.StatusBadRequest, "user not found, could not write reward")
		return
	}

	if todo.Status != "ACTIVE" {
		utils.JSONResponseError(w, http.StatusBadRequest, "todo can not be finished because it is either finished or not started yet")
		return
	}

	// calculate multiplication if frequently finished, otherwise reset
	today := time.Now()
	diff := today.Sub(todo.AvailableClaim).Hours()
	if todo.Frequency == "DAILY" {
		if today.Before(todo.AvailableClaim) {
			utils.JSONResponseError(w, http.StatusBadRequest, "this request was already marked as finished for today")
			return
		} else if diff < 24 {
			todo.Multiplication += app.cfg.MultiplicationRate
		} else {
			todo.Multiplication = 1
		}
		todo.AvailableClaim = time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 23, 59, 59, 0, time.UTC)
	} else if todo.Frequency == "TWO_DAYS" {
		if today.Before(todo.AvailableClaim) {
			utils.JSONResponseError(w, http.StatusBadRequest, "this request was already marked as finished for the last 2 days")
			return
		} else if diff < 48 {
			todo.Multiplication += app.cfg.MultiplicationRate
		} else {
			todo.Multiplication = 1
		}
		todo.AvailableClaim = time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 23, 59, 59, 0, time.UTC).AddDate(0, 0, 1)
	} else if todo.Frequency == "THREE_DAYS" {
		if today.Before(todo.AvailableClaim) {
			utils.JSONResponseError(w, http.StatusBadRequest, "this request was already marked as finished for the last three days")
			return
		} else if diff < 72 {
			todo.Multiplication += app.cfg.MultiplicationRate
		} else {
			todo.Multiplication = 1
		}
		todo.AvailableClaim = time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 23, 59, 59, 0, time.UTC).AddDate(0, 0, 2)
	} else if todo.Frequency == "WEEKLY" {
		if today.Before(todo.AvailableClaim) {
			utils.JSONResponseError(w, http.StatusBadRequest, "this request was already marked as finished for this week")
			return
		} else if diff < 168 {
			todo.Multiplication += app.cfg.MultiplicationRate
		} else {
			todo.Multiplication = 1
		}
		todo.AvailableClaim = time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 23, 59, 59, 0, time.UTC).AddDate(0, 0, 6)
	} else if todo.Frequency == "TWO_WEEKS" {
		if today.Before(todo.AvailableClaim) {
			utils.JSONResponseError(w, http.StatusBadRequest, "this request was already marked as finished for the last two weeks")
			return
		} else if diff < 336 {
			todo.Multiplication += app.cfg.MultiplicationRate
		} else {
			todo.Multiplication = 1
		}
		todo.AvailableClaim = time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 23, 59, 59, 0, time.UTC).AddDate(0, 0, 13)
	}

	// save new values
	app.db.Save(&todo)

	// calculate reward based on multiplication and quest´s values
	var gold = int(float32(todo.GoldPerCycle) * todo.Multiplication)
	var experience = int(float32(todo.ExperiencePerCycle) * todo.Multiplication)
	var reward = models.Reward{AmountOfMoney: gold, AmountOfExperience: experience}

	user.Gold += gold
	user.Experience += experience
	user.Level = int(math.Floor(app.cfg.LevelFunction*float64(user.Experience) + 1))
	app.db.Save(&user)

	// create response
	utils.JSONResponse(w, http.StatusOK, reward)
}

// Handler function for PUT /quests/todo/{todo-id}/close endpoint
// check if a quest can be closed and saved to finished quests and do so if possible
// returns
func (app *App) TodoCloseHandler(w http.ResponseWriter, r *http.Request) {
	// Get JWT claims
	claims, err := utils.GetJWTClaims(r)
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, "internal server error")
		return
	}

	todoId := mux.Vars(r)["todo-id"]

	// check user and todos
	var todo models.Todo
	app.db.Find(&todo, "id = ?", todoId)
	if todo.UserId != claims.Id {
		utils.JSONResponseError(w, http.StatusNotFound, "todo not found or no access right")
		return
	}

	if todo.Status != "ACTIVE" {
		utils.JSONResponseError(w, http.StatusBadRequest, "todo can not be completed because it is either finished or not started yet")
		return
	}

	todo.Status = "FINISHED"
	app.db.Save(&todo)

	// create response
	w.WriteHeader(http.StatusNoContent)
}
