package questService

import (
	"github.com/gorilla/mux"
	"gitlab.com/se-project/utils"
)

func GetRouter(app *App) *mux.Router {
	r := mux.NewRouter().PathPrefix("/quests").Subrouter()

	// Apply JWT checking middleware to all routes
	jwt := utils.JWTMiddleware(app.cfg.JWTSecret)
	r.Use(jwt.Handler)

	r.HandleFunc("", app.QuestListHandler).Methods("GET")
	r.HandleFunc("/todos", app.TodoListHandler).Methods("GET")
	r.HandleFunc("/todos/add/{quest-id}", app.TodoAddQuestHandler).Methods("POST")
	r.HandleFunc("/todos/{todo-id}", app.TodoDetailHandler).Methods("GET")
	r.HandleFunc("/todos/{todo-id}", app.TodoAbortHandler).Methods("DELETE")
	r.HandleFunc("/todos/{todo-id}/start", app.TodoStartHandler).Methods("PUT")
	r.HandleFunc("/todos/{todo-id}/finish", app.TodoUpdateHandler).Methods("PUT")
	r.HandleFunc("/todos/{todo-id}/close", app.TodoCloseHandler).Methods("PUT")
	r.HandleFunc("/{quest-id}", app.QuestDetailHandler).Methods("GET")

	return r
}
