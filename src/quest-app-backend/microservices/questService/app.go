package questService

import (
	"gitlab.com/se-project/config"
	"gorm.io/gorm"
)

type App struct {
	cfg *config.Config
	db  *gorm.DB
}

func NewApp(cfg *config.Config, db *gorm.DB) *App {
	return &App{
		cfg: cfg,
		db:  db,
	}
}