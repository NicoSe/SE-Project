package userService

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/se-project/utils"
)

func GetRouter(app *App) *mux.Router {
	r := mux.NewRouter().PathPrefix("/user").Subrouter()

	// Apply JWT checking middleware to all routes
	jwt := utils.JWTMiddleware(app.cfg.JWTSecret)

	r.Handle("", jwt.Handler(http.HandlerFunc(app.UserHandler))).Methods("GET")
	r.HandleFunc("", app.UserCreateHandler).Methods("POST")
	r.Handle("", jwt.Handler(http.HandlerFunc(app.UserUpdateHandler))).Methods("PUT")
	r.Handle("/byName/{playerName}", jwt.Handler(http.HandlerFunc(app.UserByPlayerNameHandler))).Methods("GET")
	r.Handle("/ranks", jwt.Handler(http.HandlerFunc(app.RankHandler))).Methods("GET")
	r.Handle("/config", jwt.Handler(http.HandlerFunc(app.ConfigHandler))).Methods("GET")
	r.Handle("/config", jwt.Handler(http.HandlerFunc(app.ConfigChangeHandler))).Methods("PUT")

	return r
}
