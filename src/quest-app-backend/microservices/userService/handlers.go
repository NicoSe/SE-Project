package userService

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/se-project/models"
	"gitlab.com/se-project/utils"
)

// Handler function for GET /user/ranks endpoint
// orders all user by their level and returns the first 20 of them for the frontend listing. Also finds the rank for the requesting user.
// returns Ranking
func (app *App) RankHandler(w http.ResponseWriter, r *http.Request) {
	// Get JWT claims
	claims, err := utils.GetJWTClaims(r)
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, "Internal server error")
		return
	}

	// Get user orderd by level
	var ranks []models.RankUser
	app.db.Model(&models.User{}).Order("level desc, updated_at").Find(&ranks)

	// Create response
	ownRank := ""
	rankUsers := make([]models.RankUser, 0)
	for nr, user := range ranks {
		if nr < 100 {
			rankUsers = append(rankUsers, user)
		}
		if user.PlayerName == claims.Username {
			ownRank = strconv.Itoa(nr + 1)
		}
	}

	ranking := models.Ranking{
		Users:   rankUsers,
		OwnRank: ownRank,
	}

	utils.JSONResponse(w, http.StatusOK, ranking)
}

// Handler function for GET /user endpoint
// find an user by id, saved in JWT return that specific user with all his details
// returns UserDetails
func (app *App) UserHandler(w http.ResponseWriter, r *http.Request) {
	// Get JWT claims
	claims, err := utils.GetJWTClaims(r)
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, "Internal server error")
		return
	}

	user := new(models.User)
	app.db.Find(&user, "id = ?", claims.Id)
	if user.ID == 0 {
		utils.JSONResponseError(w, http.StatusNotFound, "user not found")
		return
	}

	details := user.TransformToUserDetails()

	// count finished and active todos
	var activeTodos int64
	app.db.Model(&models.Todo{}).Where("user_id = ? AND status = ?", claims.Id, models.ACTIVE).Count(&activeTodos)
	var finishedTodos int64
	app.db.Model(&models.Todo{}).Where("user_id = ? AND status = ?", claims.Id, models.FINISHED).Count(&finishedTodos)
	details.ActiveQuests = int(activeTodos)
	details.FinishedQuests = int(finishedTodos)

	// calculate experience till next level
	lvl := float64(details.Level)
	exp := float64(details.Experience)
	expNext := int(lvl/app.cfg.LevelFunction - exp)
	details.ExperienceTillNextLevel = expNext

	// create response
	utils.JSONResponse(w, http.StatusOK, details)
}

// Handler function for POST /user endpoint
// create an user based on the give parameters given through request body and check if they are valid
// returns User
func (app *App) UserCreateHandler(w http.ResponseWriter, r *http.Request) {
	var newUser models.CreateUpdateUser
	err := json.NewDecoder(r.Body).Decode(&newUser)
	if err != nil {
		utils.JSONResponseError(w, http.StatusBadRequest, "JSON object is not serializable: "+err.Error())
		return
	}

	// hash password directly after decoding json, so we don't log a clear-text user password somewhere
	hashedPassword, err := utils.HashPassword(newUser.Password)
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, err.Error())
		return
	}
	newUser.Password = hashedPassword

	// check if user email already exsists
	userExist := new(models.User)
	app.db.Find(&userExist, "email = ? OR player_name = ?", newUser.Email, newUser.PlayerName)
	if userExist.ID != 0 {
		utils.JSONResponseError(w, http.StatusBadRequest, "email or playerName already exist")
		return
	}

	user := newUser.TransformToUser()

	// check if archenemy exists
	if newUser.Archenemy != "" {
		archenemyExist := new(models.User)
		app.db.Find(&archenemyExist, "player_name = ?", newUser.Archenemy)
		if archenemyExist.ID == 0 {
			utils.JSONResponseError(w, http.StatusNotFound, "archenemy not found")
			return
		}
	}

	user.Level = 1
	app.db.Save(&user)

	// create response
	log.Printf("Create user '%s'", newUser.Email)
	w.WriteHeader(http.StatusCreated)
}

// Handler function for PUT /user endpoint
// find an user by id, saved in JWT and update selected options from request body
// returns User
func (app *App) UserUpdateHandler(w http.ResponseWriter, r *http.Request) {
	// Get JWT claims
	claims, err := utils.GetJWTClaims(r)
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, "Internal server error")
		return
	}

	var updates models.CreateUpdateUser
	err = json.NewDecoder(r.Body).Decode(&updates)
	if err != nil {
		utils.JSONResponseError(w, http.StatusBadRequest, "JSON object is not serializable: "+err.Error())
		return
	}

	// find user by id
	user := new(models.User)
	app.db.Find(&user, "id = ?", claims.Id)
	if user.ID == 0 {
		utils.JSONResponseError(w, http.StatusNotFound, "user not found")
		return
	}

	if updates.Password != "" {
		// hash password directly after decoding json, so we don't log a clear-text user password somewhere
		hashedPassword, err := utils.HashPassword(updates.Password)
		if err != nil {
			utils.JSONResponseError(w, http.StatusInternalServerError, err.Error())
			return
		}
		user.Password = hashedPassword
	}
	if updates.Email != "" {
		user.Email = updates.Email
	}
	if updates.PlayerName != "" {
		user.PlayerName = updates.PlayerName
	}
	if updates.Archenemy != "" {
		// check if archenemy exists
		archenemyExist := new(models.User)
		app.db.Find(&archenemyExist, "player_name = ?", updates.Archenemy)
		if archenemyExist.ID == 0 {
			utils.JSONResponseError(w, http.StatusNotFound, "archenemy not found")
			return
		} else {
			// check if archenemy is you
			if strconv.Itoa(int(claims.Id)) == updates.Archenemy {
				utils.JSONResponseError(w, http.StatusBadRequest, "archenemy can not be you")
				return
			}
			user.Archenemy = updates.Archenemy
		}
	}

	app.db.Save(&user)

	// create response
	utils.JSONResponse(w, http.StatusOK, user)
}

// Handler function for GET /user/byName/{playername} endpoint
// find an user by his playername and returns a short info of him
// returns ShortUser
func (app *App) UserByPlayerNameHandler(w http.ResponseWriter, r *http.Request) {
	playerName := mux.Vars(r)["playerName"]

	// find user by playername
	user := new(models.User)
	app.db.Find(&user, "player_name = ?", playerName)
	if user.ID == 0 {
		utils.JSONResponseError(w, http.StatusNotFound, "user not found")
		return
	}

	// create response
	shortUser := user.TransformToShortUser()

	// count finished and active todos
	var activeTodos int64
	app.db.Model(&models.Todo{}).Where("user_id = ? AND status = ?", user.ID, models.ACTIVE).Count(&activeTodos)
	var finishedTodos int64
	app.db.Model(&models.Todo{}).Where("user_id = ? AND status = ?", user.ID, models.FINISHED).Count(&finishedTodos)
	shortUser.ActiveQuests = int(activeTodos)
	shortUser.FinishedQuests = int(finishedTodos)

	utils.JSONResponse(w, http.StatusOK, shortUser)
}

// Handler function for GET /user/config endpoint
// find config realated to user
// returns MarketConfig
func (app *App) ConfigHandler(w http.ResponseWriter, r *http.Request) {
	// Get JWT claims
	claims, err := utils.GetJWTClaims(r)
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, "Internal server error")
		return
	}

	// find config by userId
	config := new(models.UserConfig)
	app.db.Find(&config, "user_id = ?", claims.Id)
	if config.UserId == 0 {
		// if not found, create new config
		config.BackgroundImageId = "0"
		config.ProfilImageId = "0"
		config.UserId = claims.Id
		app.db.Save(&config)
	}

	// send response
	utils.JSONResponse(w, http.StatusOK, config)
}

// Handler function for PUT /user/config endpoint
// change config realated to user
// returns MarketConfig
func (app *App) ConfigChangeHandler(w http.ResponseWriter, r *http.Request) {
	// Get JWT claims
	claims, err := utils.GetJWTClaims(r)
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, "Internal server error")
		return
	}

	// deserialize body
	var updates models.UserConfig
	err = json.NewDecoder(r.Body).Decode(&updates)
	if err != nil {
		utils.JSONResponseError(w, http.StatusBadRequest, "JSON object is not serializable: "+err.Error())
		return
	}

	// find config by userId
	config := new(models.UserConfig)
	app.db.Find(&config, "user_id = ?", claims.Id)

	config.BackgroundImageId = updates.BackgroundImageId
	config.ProfilImageId = updates.ProfilImageId
	config.UserId = claims.Id
	app.db.Save(&config)

	// write response
	w.WriteHeader(http.StatusNoContent)
}
