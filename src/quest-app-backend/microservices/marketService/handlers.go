package marketService

import (
	"log"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/se-project/models"
	"gitlab.com/se-project/utils"
)

// Handler function for GET /market endpoint
// output a list of all assets the user had bought yet
// returns Inventar
func (app *App) MarketListHandler(w http.ResponseWriter, r *http.Request) {
	// Get JWT claims
	claims, err := utils.GetJWTClaims(r)
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, "Internal server error")
		return
	}

	// Get inventar of user
	var inventar models.MarketInventar
	app.db.Find(&inventar, "user_id = ?", claims.Id)
	if inventar.UserId == 0 {
		var inventarDto models.Inventar
		inventarDto.BackgroundImageInventar = make([]string, 0)
		inventarDto.ProfileImageInventar = make([]string, 0)
		utils.JSONResponse(w, http.StatusOK, inventarDto)
		return
	}

	// create response
	var inventarDto models.Inventar
	bgImageInventar := strings.Split(inventar.BackgroundImageInventar, ";")
	pImageInventar := strings.Split(inventar.ProfileImageInventar, ";")

	inventarDto.BackgroundImageInventar = bgImageInventar[:len(bgImageInventar)-1]
	inventarDto.ProfileImageInventar = pImageInventar[:len(pImageInventar)-1]

	utils.JSONResponse(w, http.StatusOK, inventarDto)
}

// Handler function for PUT /market endpoint
// add asset to inventory
func (app *App) MarketListAddHandler(w http.ResponseWriter, r *http.Request) {
	// Get JWT claims
	claims, err := utils.GetJWTClaims(r)
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, "Internal server error")
		return
	}

	price := r.URL.Query().Get("price")
	bgId := r.URL.Query().Get("backgroundImageId")
	piId := r.URL.Query().Get("profileImageId")

	// check values
	if price == "" {
		utils.JSONResponseError(w, http.StatusBadRequest, "price is missing")
		return
	}
	if bgId == "" && piId == "" {
		utils.JSONResponseError(w, http.StatusBadRequest, "there must be one filled query parameter, next to price")
		return
	}

	if bgId != "" && piId != "" {
		utils.JSONResponseError(w, http.StatusBadRequest, "there must be only one filled query parameter, next to price")
		return
	}

	// Get inventar of user
	var inventar models.MarketInventar
	app.db.Find(&inventar, "user_id = ?", claims.Id)
	inventar.UserId = claims.Id

	// add id
	if bgId != "" {
		current := strings.Split(inventar.BackgroundImageInventar, ";")
		for _, v := range current {
			if v == bgId {
				w.WriteHeader(http.StatusNoContent)
				return
			}
		}
		inventar.BackgroundImageInventar += bgId + ";"
	}
	if piId != "" {
		current := strings.Split(inventar.ProfileImageInventar, ";")
		for _, v := range current {
			if v == piId {
				w.WriteHeader(http.StatusNoContent)
				return
			}
		}
		inventar.ProfileImageInventar += piId + ";"
	}

	// check if user exists
	var user models.User
	app.db.Find(&user, "id = ?", claims.Id)
	if user.ID == 0 {
		utils.JSONResponseError(w, http.StatusNotFound, "user not found")
		return
	}

	// check prizing and amount of money
	iPrize, err := strconv.Atoi(price)

	if err != nil {
		utils.JSONResponseError(w, http.StatusBadRequest, "price not a number: "+err.Error())
		return
	}

	if iPrize < 100 {
		utils.JSONResponseError(w, http.StatusBadRequest, "prize is not fair")
		return
	}

	user.Gold -= iPrize

	if user.Gold < 0 {
		utils.JSONResponseError(w, http.StatusBadRequest, "not enough money")
		return
	}

	// save values
	app.db.Save(&inventar)
	app.db.Save(&user)
	log.Printf("User with id '%d' bought image.", claims.Id)

	// write response
	w.WriteHeader(http.StatusNoContent)
}
