package marketService

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/se-project/utils"
)

func GetRouter(app *App) *mux.Router {
	r := mux.NewRouter().PathPrefix("/market").Subrouter()

	// Apply JWT checking middleware to all routes
	jwt := utils.JWTMiddleware(app.cfg.JWTSecret)

	r.Handle("", jwt.Handler(http.HandlerFunc(app.MarketListHandler))).Methods("GET")
	r.Handle("", jwt.Handler(http.HandlerFunc(app.MarketListAddHandler))).Methods("PUT")

	return r
}
