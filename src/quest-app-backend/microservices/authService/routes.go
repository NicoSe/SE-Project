package authService

import (
	"github.com/gorilla/mux"
)

func GetRouter(app *App) *mux.Router {
	r := mux.NewRouter().PathPrefix("/auth").Subrouter()

	r.HandleFunc("/login", app.LoginHandler).Methods("POST")

	return r
}