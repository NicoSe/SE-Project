package authService

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/form3tech-oss/jwt-go"
	"gitlab.com/se-project/models"
	"gitlab.com/se-project/utils"
)

const (
	JWTExpiry = 180 * time.Hour
)

// Handler function for POST /auth/login endpoint
// login a user with email and password and return a JWT to give this user access to all other endpoints
// returns JWT
func (app *App) LoginHandler(w http.ResponseWriter, r *http.Request) {
	// Parse payload
	var cred models.Credentials
	err := json.NewDecoder(r.Body).Decode(&cred)
	if err != nil {
		utils.JSONResponseError(w, http.StatusBadRequest, err.Error())
		return
	}

	log.Printf("User with email '%s' try to login.", cred.Email)

	// Search for user via email
	var user models.User
	app.db.Find(&user, "email = ?", cred.Email)

	if user.ID == 0 {
		utils.JSONResponseError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	// Check password
	if ok, err := utils.ValidatePassword(cred.Password, user.Password); err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, err.Error())
		return
	} else if !ok {
		utils.JSONResponseError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	// Create the JWT claims, which includes the id, username and expiry time
	claims := &models.Claims{
		Id:       user.ID,
		Username: user.PlayerName,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(JWTExpiry).Unix(),
		},
	}

	// create token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(app.cfg.JWTSecret))
	if err != nil {
		utils.JSONResponseError(w, http.StatusInternalServerError, err.Error())
		return
	}

	log.Printf("User with email '%s' has logged in.", cred.Email)
	utils.JSONResponse(w, http.StatusOK, TokenResponse{
		Token: tokenString,
	})
}
