--Initial Create
CREATE DATABASE IF NOT EXISTS QuestApp;
--Grants
GRANT Usage ON *.* TO 'admin' @'%';
GRANT Create tablespace ON *.* TO 'admin' @'%';
GRANT Shutdown ON *.* TO 'admin' @'%';
GRANT Show databases ON *.* TO 'admin' @'%';
GRANT Replication slave ON *.* TO 'admin' @'%';
GRANT Process ON *.* TO 'admin' @'%';
GRANT File ON *.* TO 'admin' @'%';
GRANT Event ON *.* TO 'admin' @'%';
GRANT Reload ON *.* TO 'admin' @'%';