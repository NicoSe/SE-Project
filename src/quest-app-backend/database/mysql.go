package database

import (
	log "github.com/sirupsen/logrus"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func New(dsn string) *gorm.DB {
	log.Infof("Initialize database")
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}

	return db
}

func Migrate(db *gorm.DB, models ...interface{}) {
	log.Infof("Applying SQL migrations")

	err := db.AutoMigrate(models...)
	if err != nil {
		log.Fatalf("Error applying SQL migrations: %s", err)
	}
}