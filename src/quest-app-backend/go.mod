module gitlab.com/se-project

go 1.16

require (
	github.com/auth0/go-jwt-middleware v1.0.0
	github.com/form3tech-oss/jwt-go v3.2.2+incompatible
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/mitchellh/mapstructure v1.1.2
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.7.1
	golang.org/x/crypto v0.0.0-20190605123033-f99c8df09eb5
	gorm.io/driver/mysql v1.0.6
	gorm.io/gorm v1.21.9
)
