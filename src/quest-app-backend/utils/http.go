package utils

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/handlers"
	log "github.com/sirupsen/logrus"
	"gitlab.com/se-project/models"
)

// JSONResponse This function is a helper function to easily encode arbitray data to JSON.
// The function sets the appropriate Content-Type header, status code and does the JSON encoding.
func JSONResponse(w http.ResponseWriter, code int, data interface{}) {
	// Encode object to jsonm don't use json.NewEncoder here, as this prompts a write to the
	// response writer and thus incorrectly sets the Content-Type to "text/plain" and the
	// status to 200. This then causes a superfluous header write warning
	jsonBytes, err := json.Marshal(data)
	if err != nil {
		log.Errorf("Error serializing data: %v", err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Set HTTP headers like content type and status code
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)

	// Send actual data
	_, err = w.Write(jsonBytes)
	if err != nil {
		log.Errorf("Error sending JSON: %v", err)
	}
}

// JSONResponseError Helper function to send the Error struct.
// Error message contains error code and multiple messages.
func JSONResponseError(w http.ResponseWriter, code int, data ...string) {
	var errorType string
	if code == 500 {
		errorType = "error"
	} else {
		errorType = "warning"
	}
	JSONResponse(w, code, &models.Error{
		Code:   code,
		Type:   errorType,
		Errors: data,
	})
}

func InitCORS() func(http.Handler) http.Handler {
	return handlers.CORS(
		handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}),
		handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE", "HEAD", "OPTIONS"}),
		handlers.AllowedOrigins([]string{"*"}),
	)
}
