package utils

import (
	"net/http"

	"github.com/auth0/go-jwt-middleware"
	"github.com/form3tech-oss/jwt-go"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/se-project/models"
)

const (
	// JWTProperty This is the name of the JWT property in the request context
	JWTProperty = "jwt"
)

// JWTMiddleware Create an instance of JWT middleware with secret key.
// Purpose of this function is to act as constructor returning an initialized JWT Middleware.
func JWTMiddleware(secret string) *jwtmiddleware.JWTMiddleware {
	return jwtmiddleware.New(jwtmiddleware.Options{
		UserProperty:  JWTProperty,
		SigningMethod: jwt.SigningMethodHS256,
		// We override the default error handler to not expose any information to the client but instead, log the error
		ErrorHandler: func(w http.ResponseWriter, r *http.Request, err string) {
			log.Errorf("error decoding JWT: %v", err)
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
		},
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return []byte(secret), nil
		},
	})
}

// GetJWTClaims Extracts the Token property out of request context and decodes mapclaims into Claims.
// This function acts as a helper function to make working with Claims inside our handlers easier.
// Using the mapclaims data type is not as handy as working directly with the claims struct.
func GetJWTClaims(r *http.Request) (*models.Claims, error) {
	// Extract `jwt.Token` out of request context
	token := r.Context().Value(JWTProperty).(*jwt.Token)

	// Initialize Claims variable and decode mapclaims into Claims struct
	claims := new(models.Claims)
	if err := mapstructure.Decode(token.Claims, &claims); err != nil {
		log.Errorf("failed decoding JWT claims: %v", err)
		return nil, err
	}

	return claims, nil
}
