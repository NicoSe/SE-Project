package utils

import (
	"crypto/rand"
	"crypto/subtle"
	"encoding/base64"
	"fmt"
	"strings"

	"golang.org/x/crypto/argon2"
)

const (
	ARGON_TIME       = 1
	ARGON_MEMORY     = 64 * 1024
	ARGON_THREADS    = 4
	ARGON_KEY_LENGTH = 32
)

func HashPassword(password string) (string, error) {
	salt := make([]byte, 16)
	if _, err := rand.Read(salt); err != nil {
		return "", err
	}

	hash := argon2.IDKey([]byte(password), salt, ARGON_TIME, ARGON_MEMORY, ARGON_THREADS, ARGON_KEY_LENGTH)

	return fmt.Sprintf("$argon2id$v=%d$m=%d,t=%d,p=%d$%s$%s",
			argon2.Version,
			ARGON_MEMORY, ARGON_TIME, ARGON_THREADS,
			base64.RawStdEncoding.EncodeToString(salt),
			base64.RawStdEncoding.EncodeToString(hash)),
		nil
}

func ValidatePassword(password, hashedPassword string) (bool, error) {
	argonParts := strings.Split(hashedPassword, "$")

	var memory, time, threads uint32

	_, err := fmt.Sscanf(argonParts[3], "m=%d,t=%d,p=%d", &memory, &time, &threads)
	if err != nil {
		return false, err
	}

	salt, err := base64.RawStdEncoding.DecodeString(argonParts[4])
	if err != nil {
		return false, err
	}

	decodedHash, err := base64.RawStdEncoding.DecodeString(argonParts[5])
	if err != nil {
		return false, err
	}
	keyLength := len(decodedHash)

	comparisonHash := argon2.IDKey([]byte(password), salt, time, memory, uint8(threads), uint32(keyLength))

	return subtle.ConstantTimeCompare(decodedHash, comparisonHash) == 1, nil
}
